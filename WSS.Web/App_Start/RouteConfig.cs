﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WSS.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //chenrh
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            routes.MapRoute(
               "default",
               "{controller}/{action}/{id}",
               new { controller = "Login", action = "Login", guid = UrlParameter.Optional, id = UrlParameter.Optional }
           );
        }
    }
}