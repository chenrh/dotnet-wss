﻿/*
 jQuery 修复IE8及以下IE bootstrap 删格化 排版问题
 
author:         陈日红@杭州
date:           2014‎年‎4‎月‎13‎日 9:42:22
version:        v1.0

<!--[if lte IE 8]>
    <script src="@Url.Content("~/Scripts/plugins/jquery.fixed.ie.js")"></script>
<![endif]-->
*/
; (function ($) {
    $.fn.extend({
        just_fix_col: function () {
            this.each(function () {
                var $row = $(this).children("div");
                var size = $row.size(),ieverson = $.browser.version;
                
                $row.each(function (idx, divobj) {
                    (idx + 1) == size && ieverson < 8 && $(divobj).remove();

                    var clsname = $(divobj).attr("class");
                    if (clsname) {
                        var arr = clsname.split(' ')
                        , colwidth;
                        if (arr.length == 1) {
                            colwidth = clsname.slice(-1);
                        } else {
                            colwidth = arr[0].split('-').slice(-1);
                        }
                        if (isNaN(parseInt(colwidth, 10))) {
                            alert('未找到 [ ' + clsname + " ]的定义");
                        } else {
                            if (ieverson > 7)
                                $(divobj).addClass("ie8_row_col_" + colwidth);
                            else {
                                if (colwidth != 1) {
                                    $(divobj).addClass("ie8_row_col_" + colwidth);
                                } else {
                                    $(divobj).addClass("ie7_row_col_" + colwidth);
                                }
                            }
                        }
                    }
                });
            });
        }
    });
})(jQuery);

function just_fix_lte_ie8() {
    $(".row").size() > 0 && $(".row").just_fix_col();
    $(".container:first").css("width", "80%");
}

just_fix_lte_ie8();