﻿/*
 * 瀑布流加载方式 陈日红 2014-4-15 于杭州
 * 
 * 当滚动条往下拉到高度小于151px时，执行回调函数，用于加载数据
 */
function Waterfall(callback) {

    var self = this;

    window.onscroll = function () {
        self.onClientScrolled(callback);
    };
}

Waterfall.prototype.onClientScrolled = function (callback) {
    var bodyheight_element = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    var bodyheight_view = document.documentElement.clientHeight;
    var srolltop = document.documentElement.scrollTop || document.body.scrollTop;
    var x = (bodyheight_element - bodyheight_view - srolltop);
    if (x < 151) {
        callback && callback();
    }
};

Waterfall.prototype.cancelListen = function() {
    window.onscroll = null;
};

/* 调用方式
    <script type="text/javascript">
        $(function () {

            function get_resource() {
                $.post("/rescenter/list", {}, function (msg) {
                    if (msg && msg.data.length) {
                        var $content = $("#rescontent blockquote .list-group");
                        for (var i = 0; i < msg.data.length; i++) {
                            var json = msg.data[i];
                            $('<a href="' + json.Path + '" class="list-group-item">' + json.Title + '</a>')
                                .css({ "opacity": "0" }).appendTo($content).stop().animate({
                                    "opacity": "1"
                                }, 1000);
                        }
                    }
                });
            }

            var a = new Waterfall(get_resource);

            get_resource();
        });
    </script>
*/