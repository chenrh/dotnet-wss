﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSS.Models.Hosp;
using WSS.Pub;

namespace WSS.Web.Controllers
{
    public class HospitalController : MyController
    {

        public ActionResult add()
        {
            
            WSS.Models.Hosp.HOSP_HOSPITAL model = new HOSP_HOSPITAL().Bind<HOSP_HOSPITAL>(Request);
            if (model.HOSP_NAME.IsNullOrEmpty())
            {
                return this.MyJson(false, "医院名称不能为空");
            }

            model.HOSP_ID = BLL.Xt.XhbManager.Instance.GetXh(model.GetTableName());
            WSS.BLL.Hospital.HospDB.Instance.Insert(model);

            return this.MyJson(true, model.HOSP_NAME + " 添加成功!");

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult List()
        {
            var param = new StrObjDict();
            var sch_hospname = Request.Req("sch_hospname");
            if (!sch_hospname.IsNullOrEmpty())
            {
                param.Merger(new { WHERE_CLAUSE = "HOSP_NAME LIKE '%" + sch_hospname + "%'" });
            }

            param.Merger(new { ORDER_BY_CLAUSE = " HOSP_ID DESC" });

            var data = DB.ListSod("LIST2_HOSP_HOSPITAL",param);

            return this.MyJson(Paginator.Page<StrObjDict>(data));
        }
    }
}
