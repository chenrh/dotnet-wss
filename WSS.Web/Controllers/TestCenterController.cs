﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using WSS.BLL.Test;
using WSS.BLL.User;
using WSS.Models.Test;
using WSS.Models.User;
using WSS.Pub;

namespace WSS.Web.Controllers
{
    public class TestCenterController : MyController
    {

        #region 试卷分类

        /// <summary>
        /// 试卷分类维护
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult TestCategory()
        {
            return View();
        }

        /// <summary>
        /// 试卷分类数据
        /// </summary>
        /// <returns></returns>
        public ContentResult GetTestCategory()
        {
            var list =
                TestCateDb.Instance.GetList(new { ORDER_BY_CLAUSE = "CATE_LEVEL,ORDER_NO" });
            StringBuilder html = new StringBuilder();
            if (list.Count > 0)
            {
                get_test_cate_grid(ref html, "ROOT", list);
            }
            return Content(html.ToString());
        }
        /// <summary>
        /// 试卷分类数据(用于select下拉数据绑定)
        /// </summary>
        /// <returns></returns>
        public ContentResult GetTestCategoryToSelect()
        {
            var list =
                TestCateDb.Instance.GetList(new { FALG_INVALID = 0, ORDER_BY_CLAUSE = "CATE_LEVEL,ORDER_NO" });
            StringBuilder html = new StringBuilder();
            if (list.Count > 0)
            {
                string valControlId = Request.Req("valControlID");
                string textControlId = Request.Req("textControlID");
                if (Request.Req("addData") == "1")
                {
                    html.AppendLine("<li><a href=\"#\" onclick='onClickMenu(\"" + textControlId + "\",\"\",\"无\",\"" + valControlId + "\")'>无</a></li>");
                    html.AppendLine("<li class=\"divider\"></li>");
                }
                if (Request.Req("selectall") == "1")
                {
                    html.AppendLine("<li><a href=\"#\" onclick='onClickMenu(\"" + textControlId + "\",\"\",\"全部\",\"" + valControlId + "\")'>全部</a></li>");
                    html.AppendLine("<li class=\"divider\"></li>");
                }
                get_test_cate_select(ref html, "ROOT", list, valControlId, textControlId);
            }
            return Content(html.ToString());
        }

        /// <summary>
        /// 列表数据维护
        /// </summary>
        /// <param name="html"></param>
        /// <param name="parentno"></param>
        /// <param name="datas"></param>
        private void get_test_cate_grid(ref StringBuilder html, string parentno, IEnumerable<TEST_CATEGORY> datas)
        {
            var subdatas = datas.Where(t => t.CATE_PNO == parentno).ToList();

            foreach (TEST_CATEGORY category in subdatas)
            {
                html.AppendLine("<li class=\"list-group-item\" style=\"margin-left:" + category.CATE_LEVEL * 50 +
                                "px;\" data-cateno=\"" + category.CATE_NO + "\" data-lvl=\"" + category.CATE_LEVEL +
                                "\">");
                html.AppendLine("<label>" + category.CATE_NAME + "</label>");
                html.AppendLine("<span class=\"pull-right close\" onclick=\"removecate(this);\">&times;</span >");
                html.AppendLine("</li>");

                if (category.FLAG_LAST != 1)
                {
                    get_test_cate_grid(ref html, category.CATE_NO, datas);
                }
            }
        }

        /// <summary>
        /// 下拉菜单
        /// </summary>
        /// <param name="html"></param>
        /// <param name="parentno"></param>
        /// <param name="datas"></param>
        private void get_test_cate_select(ref StringBuilder html, string parentno, IEnumerable<TEST_CATEGORY> datas, string valControlId, string textControlId)
        {
            var subdatas = datas.Where(t => t.CATE_PNO == parentno);

            foreach (TEST_CATEGORY category in subdatas)
            {
                html.AppendLine("<li CateNo=\"" + category.CATE_NO + "\">");
                html.AppendLine("<a href='#' style='margin-left:" + category.CATE_LEVEL * 10 + "px;' " +
                    "onclick='onClickMenu(\"" + textControlId + "\",\"" + category.CATE_NO + "\",\"" + category.CATE_NAME + "\",\"" + valControlId + "\")'>" + category.CATE_NAME + "</a>");
                html.AppendLine("</li>");

                if (category.FLAG_LAST != 1)
                {
                    get_test_cate_select(ref html, category.CATE_NO, datas, valControlId, textControlId);
                }
            }
        }

        /// <summary>
        /// 增加试卷分类
        /// </summary>
        /// <returns></returns>
        public ActionResult AddCategory()
        {
            var order = Request.Req("orderno");
            var catename = Request.Req("catename");
            var parentno = Request.Req("parentno");
            var level = Request.Req("level");

            if (catename.IsNullOrEmpty() || parentno.IsNullOrEmpty() || order.IsNullOrEmpty())
            {
                return this.MyJson(false, "分类新增失败，缺少参数");
            }

            var listdb = new List<DBState>();

            TEST_CATEGORY model = new TEST_CATEGORY();
            model.CATE_NAME = Request.Req("catename");
            model.ORDER_NO = order.IsNumeric() ? int.Parse(order) : 12;
            model.CATE_PNO = parentno;
            model.FLAG_INVALID = 0;
            model.CATE_NO = BLL.Xt.XhbManager.Instance.GetXh(model.GetTableName());
            model.USER_ID = AccountCache.UserId;
            model.FLAG_LAST = 1;
            model.CATE_LEVEL = int.Parse(level);

            if (model.CATE_PNO != "ROOT")
            {
                TEST_CATEGORY parentmodel = TestCateDb.Instance.GetOne(model.CATE_PNO);
                parentmodel.FLAG_LAST = 0;
                listdb.Add(new DBState { Name = parentmodel.MAP_UPDATE, Param = parentmodel, Type = ESqlType.UPDATE });
            }
            listdb.Add(new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT });
            DB.Execute(listdb);

            return this.MyJson(true, model.CATE_NO);
        }

        /// <summary>
        /// 重命名试卷分类
        /// </summary>
        /// <returns></returns>
        public ActionResult RenameCategory()
        {
            string cateno = Request.Req("cateno"),
                newname = Request.Req("newname");

            try
            {
                var dbinstance = TestCateDb.Instance;
                var model = dbinstance.GetOne(cateno);
                model.CATE_NAME = newname;
                dbinstance.Update(model);
                return this.MyJson(true, "分类重命名成功");
            }
            catch (Exception ex)
            {
                return this.MyJson(false, "分类重命名失败");
            }
        }

        /// <summary>
        /// 移除试卷分类
        /// </summary>
        /// <returns></returns>
        public ActionResult RemoveCategory()
        {
            string cateno = Request.Req("cateno");

            try
            {
                var dbinstance = TestCateDb.Instance;
                var userid = AccountCache.UserId;

                var model = dbinstance.GetOne(cateno);

                if (model == null || model.CATE_NO == null)
                {
                    return this.MyJson(false, "分类不存在，刷新页面后再看看吧");
                }

                if (model.USER_ID != userid)
                {
                    return this.MyJson(false, "大哥，你删错对象了，别乱改参数啊");
                }

                if (model.FLAG_LAST == 0)
                {
                    return this.MyJson(false, "只能移除末级的分类");
                }

                var listres = TestDb.Instance.GetList(new { CATE_NO = cateno });
                if (listres.Count > 0)
                {
                    return this.MyJson(false, "该分类下有" + listres.Count + "套试卷，不允许删除!");
                }

                var listdb = new List<DBState>();

                if (model.CATE_PNO != "ROOT")
                {
                    var siblings = dbinstance.GetList(new { CATE_NO = model.CATE_PNO, FLAG_INVALID = 0, USER_ID = userid });
                    if (siblings.Count == 1)
                    {
                        var paentmodel = dbinstance.GetOne(model.CATE_PNO);
                        paentmodel.FLAG_LAST = 1;
                        listdb.Add(new DBState
                        {
                            Name = paentmodel.MAP_UPDATE,
                            Param = paentmodel,
                            Type = ESqlType.UPDATE
                        });
                    }
                }
                listdb.Add(new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE });

                DB.Execute(listdb);

                return this.MyJson(true, "分类移除成功");
            }
            catch (Exception ex)
            {
                return this.MyJson(false, "分类移除失败");
            }
        }

        #endregion

        #region 试题类别

        /// <summary>
        /// 试题类别维护
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult TestQCategory()
        {
            return View();
        }

        /// <summary>
        /// 试题类别数据
        /// </summary>
        /// <returns></returns>
        public ContentResult GetTestQCategory()
        {

            var list = TestQCateDb.Instance.GetList(new { ORDER_BY_CLAUSE = "ORDER_NO" });
            StringBuilder html = new StringBuilder();
            if (list.Count > 0)
            {
                foreach (TEST_QUESTION_CATEGORY category in list)
                {
                    html.AppendLine("<li class=\"list-group-item\" data-cateno=\"" + category.CATE_NO + "\">");
                    html.AppendLine("<label>" + category.CATE_NAME + "</label>");
                    html.AppendLine("<span class=\"pull-right close\" onclick=\"removecate(this);\">&times;</span >");
                    html.AppendLine("</li>");
                }
            }
            return Content(html.ToString());
        }
        /// <summary>
        /// 试题类别绑定到select
        /// </summary>
        /// <returns></returns>
        public ContentResult GetTestQCateToSelect()
        {
            var list = TestQCateDb.Instance.GetList(new { FALG_INVALID = 0, ORDER_BY_CLAUSE = "ORDER_NO" });
            StringBuilder html = new StringBuilder();
            if (list.Count > 0)
            {
                if (Request.Req("addData") == "1")
                {
                    html.AppendLine("<li><a href=\"#\" onclick='onClickMenu(\"\",\"请选择\")'>请选择</a></li>");
                    html.AppendLine("<li class=\"divider\"></li>");
                }
                if (Request.Req("selectall") == "1")
                {
                    html.AppendLine("<li><a href=\"#\" onclick='onClickMenu(\"\",\"全部\")'>全部</a></li>");
                    html.AppendLine("<li class=\"divider\"></li>");
                }
                foreach (TEST_QUESTION_CATEGORY category in list)
                {
                    html.AppendLine("<li CateNo=\"" + category.CATE_NO + "\">");
                    html.AppendLine("<a href='#'  " +
                        "onclick='onClickMenu(\"" + category.CATE_NO + "\",\"" + category.CATE_NAME + "\")'>" + category.CATE_NAME + "</a>");
                    html.AppendLine("</li>");
                }
            }
            return Content(html.ToString());
        }

        /// <summary>
        /// 增加试题类别
        /// </summary>
        /// <returns></returns>
        public ActionResult AddQCategory()
        {
            var order = Request.Req("orderno");
            var catename = Request.Req("catename");

            if (catename.IsNullOrEmpty() || order.IsNullOrEmpty())
            {
                return this.MyJson(false, "分类新增失败，缺少参数");
            }

            var listdb = new List<DBState>();

            TEST_QUESTION_CATEGORY model = new TEST_QUESTION_CATEGORY();
            model.CATE_NAME = Request.Req("catename");
            model.ORDER_NO = order.IsNumeric() ? int.Parse(order) : 12;
            model.CATE_NO = BLL.Xt.XhbManager.Instance.GetXh(model.GetTableName());

            listdb.Add(new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT });
            DB.Execute(listdb);

            return this.MyJson(true, model.CATE_NO);
        }

        /// <summary>
        /// 重命名试题类别
        /// </summary>
        /// <returns></returns>
        public ActionResult RenameQCategory()
        {
            string cateno = Request.Req("cateno"),
                newname = Request.Req("newname");

            try
            {
                var dbinstance = TestQCateDb.Instance;
                var model = dbinstance.GetOne(cateno);
                model.CATE_NAME = newname;
                dbinstance.Update(model);
                return this.MyJson(true, "分类重命名成功");
            }
            catch (Exception ex)
            {
                return this.MyJson(false, "分类重命名失败");
            }
        }

        /// <summary>
        /// 移除试题类别
        /// </summary>
        /// <returns></returns>
        public ActionResult RemoveQCategory()
        {
            string cateno = Request.Req("cateno");

            try
            {
                var dbinstance = TestQCateDb.Instance;

                var model = dbinstance.GetOne(cateno);

                if (model == null || model.CATE_NO == null)
                {
                    return this.MyJson(false, "分类不存在，刷新页面后再看看吧");
                }

                var listres = TestQDb.Instance.GetList(new { CATE_NO = cateno });
                if (listres.Count > 0)
                {
                    return this.MyJson(false, "该分类下有" + listres.Count + "道试题，不允许删除!");
                }

                var listdb = new List<DBState>();

                listdb.Add(new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE });

                DB.Execute(listdb);

                return this.MyJson(true, "分类移除成功");
            }
            catch (Exception ex)
            {
                return this.MyJson(false, "分类移除失败");
            }
        }

        #endregion

        #region 试卷维护
        [MyAuthorize]
        public ActionResult TestList()
        {
            string sqlStr = "select max(to_number(ORDER_NO)) maxQNo from TEST_TEST";
            ViewBag.OrderNO = Convert.ToInt32(DB.SelectFirst(sqlStr)) + 1;
            ViewBag.CurYear = DateTime.Now.Year;
            return View();
        }
        /// <summary>
        /// 试卷列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetTestList()
        {
            var param = new StrObjDict();

            var testName = Request.Req("sch_testname");
            string whereStr = "";
            if (!testName.IsNullOrEmpty())
            {
                whereStr += "TEST_NAME LIKE '%" + testName + "%'";
            }
            var cateNo = Request.Req("cateno");
            if (!cateNo.IsNullOrEmpty())
            {
                whereStr += whereStr != "" ? " and " : "";
                whereStr += "CATE_NO = '" + cateNo + "'";
            }
            var flagInvaild = Request.Req("flagInvaild");
            if (!flagInvaild.IsNullOrEmpty())
            {
                whereStr += whereStr != "" ? " and " : "";
                whereStr += "FLAG_INVALID = " + flagInvaild + "";    
            }
            if (whereStr != "")
            {
                param.Merger(new { WHERE_CLAUSE = whereStr });
            }
            param.Merger(new { ORDER_BY_CLAUSE = " ORDER_NO ", UPLOAD_USERID =AccountCache.UserId});

            IList<StrObjDict> datas = DB.ListSod("LIST3_TEST_TEST", param);
            return this.MyJson(Paginator.Page<StrObjDict>(datas));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult GetOneTestModel()
        {
            var data = TestDb.Instance.GetOne(Request.Req("id"));
            return this.MyJson(data);
        }

        /// <summary>
        /// 试卷新增与维护
        /// </summary>
        /// <param name="testModel"></param>
        /// <returns></returns>
        public ActionResult SaveTest(TEST_TEST testModel)
        {
            if (string.IsNullOrEmpty(testModel.TEST_ID))
            {
                testModel.TEST_ID = BLL.Xt.XhbManager.Instance.GetXh(testModel.GetTableName());
                testModel.ORDER_NO = testModel.ORDER_NO == "" ? "0" : testModel.ORDER_NO;
                testModel.READCOUNT = 0;
                testModel.UPLOAD_TIME = DateTime.Now;
                testModel.UPLOAD_USERID = AccountCache.UserId;
                testModel.HOSP_ID = AccountCache.CurrentUserInfo.UserInfo.HOSP_ID;
                TestDb.Instance.Insert(testModel);
                return this.MyJson(true, "新增成功");
            }
            else
            {
                var model = TestDb.Instance.GetOne(Request.Req("TEST_ID"));
                if (model != null)
                {
                    testModel.ORDER_NO = testModel.ORDER_NO == "" ? model.ORDER_NO : testModel.ORDER_NO;
                    testModel.READCOUNT = model.READCOUNT;
                    testModel.UPLOAD_TIME = DateTime.Now;
                    testModel.UPLOAD_USERID = AccountCache.UserId;
                    testModel.HOSP_ID = AccountCache.CurrentUserInfo.UserInfo.HOSP_ID;
                    TestDb.Instance.Update(testModel);
                }
                return this.MyJson(true, "编辑成功");
            }
        }
        private string SplitIds(string[] ids)
        {

            string ret = "";
            if (ids != null && ids.Length > 0)
            {
                foreach (string id in ids)
                {
                    ret += "'" + id + "'" + ",";
                }
            }
            return ret != "" ? ret.Substring(0, ret.Length - 1) : "''";
        }
        /// <summary>
        /// 删除试卷
        /// </summary>
        /// <returns></returns>
        public JsonResult DelTest()
        {
            try
            {
                string[] testId = Request.Req("id").Split(',');
                List<DBState> db = new List<DBState>();
                db.Add(new DBState { Name = "DELETE_TEST_ByWhere", Param = new { WHERE_CLAUSE = " test_id in (" + SplitIds(testId) + ")" }, Type = ESqlType.DELETE });
                db.Add(new DBState { Name = "DELETE_TEST_QUESTION_BYWHERE", Param = new { WHERE_CLAUSE = " TEST_ID in (" + SplitIds(testId) + ")" }, Type = ESqlType.DELETE });
                DB.Execute(db);
                return this.MyJson(true, "删除成功");
            }
            catch
            {
                return this.MyJson(true, "数据操作异常！");
            }
        }



        #endregion

        #region 试题维护
        [MyAuthorize]
        public ActionResult TestQList()
        {
            string testId = Request.Req("testId");
            var data = TestDb.Instance.GetOne(testId);
            ViewBag.TestID = testId;
            ViewBag.TestName = data.TEST_NAME;
            ViewBag.QCateList = DB.ListSod("LIST3_TEST_QUESTION_CATEGORY", new { TEST_ID = testId });
            return View();
        }
        /// <summary>
        /// 试题维护
        /// </summary>
        /// <returns></returns>
        public ActionResult TestQForm()
        {
            string id = Request.Req("id");
            ViewBag.TestID = Request.Req("testId");
            ViewBag.QUESTION_ID = id;
            ViewBag.AnswerList = (List<TEST_ANSWER>)QAnswerDb.Instance.GetList(new { Question_ID = id, ORDER_BY_CLAUSE = "ANSWER_NO" });
            //获取当前试题数据的最大序号
            string sqlStr = "select max(to_number(question_no)) maxQNo from test_question";
            ViewBag.QNewNo = Convert.ToInt32(DB.SelectFirst(sqlStr)) + 1;
            ViewBag.QCateNo = Request.Req("cateNo");
            return View("Form_TestQuestion");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult GetOneTestQModel()
        {
            var data = TestQDb.Instance.GetOne(Request.Req("id"));
            return this.MyJson(data);
        }

        /// <summary>
        /// 试题新增与维护
        /// </summary>
        /// <returns></returns>
        public ActionResult SaveTestQ()
        {
            try
            {
                TEST_QUESTION testQModel = new TEST_QUESTION().Bind<TEST_QUESTION>(Request);
                object[] answerArr = (object[])JsonAdapter.FromJsonAsDictionary(Request.Req("answer"));
                List<DBState> db = new List<DBState>();

                if (string.IsNullOrEmpty(testQModel.QUESTION_ID))
                {
                    testQModel.QUESTION_ID = BLL.Xt.XhbManager.Instance.GetXh(testQModel.GetTableName());
                    //增加对应答案项
                    foreach (IDictionary<string, object> answer in answerArr)
                    {
                        TEST_ANSWER aModel = new TEST_ANSWER();
                        aModel.ANSWER_ID = BLL.Xt.XhbManager.Instance.GetXh(aModel.GetTableName());
                        aModel.ANSWER_NO = Utils.GetString(answer["ANSWER_NO"]);
                        aModel.CONTENTS = Utils.GetString(answer["CONTENTS"]);
                        aModel.FLAG_INVALID = 0;
                        aModel.FLAG_ISRIGHT = Convert.ToInt32(answer["FLAG_ISRIGHT"]);
                        aModel.QUESTION_ID = testQModel.QUESTION_ID;
                        db.Add(new DBState { Name = "INSERT_TEST_ANSWER", Param = aModel.ToDict(), Type = ESqlType.INSERT });
                    }
                    db.Add(new DBState { Name = "INSERT_TEST_QUESTION", Param = testQModel.ToDict(), Type = ESqlType.INSERT });
                    DB.Execute(db);
                    return this.MyJson(true, "新增成功");
                }
                else
                {
                    db.Add(new DBState { Name = "DELETE_TEST_ANSWER_BYQID", Param = new { QUESTION_ID = testQModel.QUESTION_ID }, Type = ESqlType.DELETE });

                    var model = TestQDb.Instance.GetOne(Request.Req("QUESTION_ID"));
                    if (model != null)
                    {
                        db.Add(new DBState { Name = "UPDATE_TEST_QUESTION", Param = testQModel.ToDict(), Type = ESqlType.UPDATE });
                    }
                    //增加对应答案项
                    foreach (IDictionary<string, object> answer in answerArr)
                    {
                        TEST_ANSWER aModel = new TEST_ANSWER();
                        aModel.ANSWER_ID = BLL.Xt.XhbManager.Instance.GetXh(aModel.GetTableName());
                        aModel.ANSWER_NO = Utils.GetString(answer["ANSWER_NO"]);
                        aModel.CONTENTS = Utils.GetString(answer["CONTENTS"]);
                        aModel.FLAG_INVALID = 0;
                        aModel.FLAG_ISRIGHT = Convert.ToInt32(answer["FLAG_ISRIGHT"]);
                        aModel.QUESTION_ID = testQModel.QUESTION_ID;
                        db.Add(new DBState { Name = "INSERT_TEST_ANSWER", Param = aModel.ToDict(), Type = ESqlType.INSERT });
                    }
                    DB.Execute(db);
                    return this.MyJson(true, "编辑成功");
                }
            }
            catch
            {
                return this.MyJson(false, "数据操作异常");
            }

        }
        /// <summary>
        /// 删除试题 
        /// </summary>
        /// <returns></returns>
        public JsonResult DelTestQ()
        {
            try
            {
                DBState db = new DBState { Name = "DELETE_TEST_QUESTION", Param = new { QUESTION_ID = Request.Req("id") }, Type = ESqlType.DELETE };
                DB.Execute(db);
                return this.MyJson(true, "删除成功");
            }
            catch
            {
                return this.MyJson(false, "数据操作异常！");
            }
        }

        public ActionResult GetQuestionList()
        {
            var param = new StrObjDict();
            string cateNo = Request.Req("cateNo");
            string testId = Request.Req("testId");
            if (string.IsNullOrEmpty(cateNo))
            {
                param.Merger(new { WHERE_CLAUSE = "TEST_ID=  '" + testId + "' and (CATE_NO='' or CATE_NO is null)" });
            }
            else
            {
                param.Merger(new { WHERE_CLAUSE = "TEST_ID=  '" + testId + "' and CATE_NO = '" + cateNo + "'" });
            }
            param.Merger(new { ORDER_BY_CLAUSE = "question_no" });


            IDictionary<List<TEST_QUESTION>, IDictionary<TEST_QUESTION, List<TEST_ANSWER>>> retList =
               new Dictionary<List<TEST_QUESTION>, IDictionary<TEST_QUESTION, List<TEST_ANSWER>>>();

            IDictionary<TEST_QUESTION, List<TEST_ANSWER>> list = new Dictionary<TEST_QUESTION, List<TEST_ANSWER>>();

            var questionData = (List<TEST_QUESTION>)TestQDb.Instance.GetList(param);

            var quesid = "";
            foreach (TEST_QUESTION question in questionData)
            {
                quesid += string.Format(" QUESTION_ID = '{0}' OR", question.QUESTION_ID);
            }

            quesid = !quesid.IsNullOrEmpty() ? "(" + quesid.TrimEnd('R').TrimEnd('O') + ")" : "1=1";

            var answer_list = QAnswerDb.Instance.GetList(new { WHERE_CLAUSE = quesid, ORDER_BY_CLAUSE = "ANSWER_NO" });

            foreach (var model in questionData)
            {
                var qid = model.QUESTION_ID;
                var answers = answer_list.Where(t => t.QUESTION_ID == qid).ToList();
                list.Add(model, answers);
            }
            retList.Add(questionData, list);

            ViewBag.QList = retList;
            return View("QDataList");
        }
        /// <summary>
        /// 设置试题题型
        /// </summary>
        /// <returns></returns>
        public JsonResult SetQuestionType()
        {
            try
            {
                TEST_QUESTION model = TestQDb.Instance.GetOne(Request.Req("id"));
                model.QUESTION_TYPE = Convert.ToInt32(Request.Req("type"));
                TestQDb.Instance.Update(model);
                return this.MyJson(true, "题型设置成功");
            }
            catch
            {
                return this.MyJson(false,"数据操作异常");
            }
        }
        /// <summary>
        /// 设置试题的正确答案
        /// </summary>
        /// <returns></returns>
        public JsonResult SetRightAnswer()
        {
            try
            {
                TEST_ANSWER model = QAnswerDb.Instance.GetOne(Request.Req("id"));
                model.FLAG_ISRIGHT = Convert.ToInt32(Request.Req("isRight"));
                QAnswerDb.Instance.Update(model);
                return this.MyJson(true, "正确答案设置成功");
            }
            catch
            {
                return this.MyJson(false, "数据操作异常");
            }
        }

        #endregion

        #region 试卷导入

        public ActionResult Import()
        {
            return View();
        }

        public ActionResult CheckImportFile()
        {
            var filename = Request.Req("filename");

            if (filename.IsNullOrEmpty())
            {
                return this.MyJson(false, "缺少参数:文件名");
            }

            try
            {
                BLL.Test.TestImport imp = new TestImport(filename, AccountCache.UserId);

                return this.MyJson(true, "导入成功");
            }
            catch (Exception exception)
            {

                return this.MyJson(false, exception.Message);
            }
        }
        #endregion
    }
}
