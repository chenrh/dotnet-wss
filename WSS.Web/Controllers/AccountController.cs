﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WSS.BLL.Account;
using WSS.Pub;

namespace WSS.Web.Controllers
{
    public class AccountController : MyController
    {
        public ActionResult Onlines()
        {
            var list = WSS.BLL.Account.UserOnline.OnlineList;
            return Json(new { data = list });
        }


        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            string guid = this.AccountCache.CurrentUserInfo.Guid.ToString();
            new LoginHelper().Logout(guid);
            HttpCookie auth_cookie = new HttpCookie(FormsAuthentication.FormsCookieName, null);
            Response.Cookies.Add(auth_cookie);

            return RedirectToAction("Login", "Login");
        }

    }
}
