﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WSS.BLL.Resource;
using WSS.BLL.User;
using WSS.Models.Hosp;
using WSS.Pub;
using WSS.Models.User;

namespace WSS.Web.Controllers
{
    public class AdmController : MyController
    {
        //
        // GET: /Adm/

        /// <summary>
        /// 后台首页2
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult Index()
        {
            string userpic = this.AccountCache.CurrentUserInfo.UserInfo.USER_PIC;

            ViewBag.USERPIC = userpic;

            return View();
        }

        [MyAuthorize]
        public ActionResult IndexV2()
        {
            string userpic = this.AccountCache.CurrentUserInfo.UserInfo.USER_PIC;
            ViewBag.USERPIC = userpic;

            var sql = @"SELECT CD_ID,CD_PID,CD_MC,CD_GNID,CD_BKCOLOR,CD_ICON,CD_LEVEL,'/'||GN.GN_CONTROLLER||'/'||GN.GN_ACTION URL FROM XT_CDB CD 
LEFT JOIN XT_GNB GN ON CD.CD_GNID = GN.GN_ID ORDER BY CD_LEVEL,ORDER_NO ASC";
            var listmenu = DB.Select(sql);

            var rootmenu = listmenu.Where(t => Utils.GetString(t["CD_LEVEL"]) == "0").ToList();
            var submenu = listmenu.Where(t => Utils.GetString(t["CD_LEVEL"]) == "1").ToList();

            ViewBag.ROOTMENU = rootmenu;
            ViewBag.SUBMENU = submenu;

            return View();
        }


        /// <summary>
        /// 后台首页主页 个人中心
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult Home()
        {
            return View();
        }

        /// <summary>
        /// 用户中心
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult UserCenter()
        {
            return View();
        }

        /// <summary>
        /// 资源中心
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult ResCenter()
        {
            return View();
        }

        [MyAuthorize]
        public ActionResult Onlines()
        {
            return View();
        }


        /// <summary>
        /// 测试
        /// </summary>
        /// <returns></returns>
        public ActionResult test()
        {
            return View();
        }
        /// <summary>
        /// 个人设置
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult PersonalSetting()
        {
            string HospID = AccountCache.CurrentUserInfo.UserInfo.HOSP_ID;
            string UserID = AccountCache.CurrentUserInfo.UserId;
            string UserNo = AccountCache.CurrentUserInfo.UserInfo.USER_NO;

            var CurUserInfo = new User(HospID).GetOne(UserID, UserNo);
            ViewData.Model = CurUserInfo;
            return View();
        }

        /// <summary>
        /// 题库中心
        /// </summary>
        /// <returns></returns>
        [MyAuthorize]
        public ActionResult TestCenter()
        {
            return View();
        }
    }
}
