﻿using WSS.Models.Hosp;
using WSS.Pub;

namespace WSS.BLL.Hospital
{
    /// <summary>
    /// 
    /// </summary>
    public class HospDB
    {
        private static readonly HospDB _Instance;

        private HospDB() { }

        static HospDB()
        {
            _Instance = new HospDB();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(WSS.Models.Hosp.HOSP_HOSPITAL model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(WSS.Models.Hosp.HOSP_HOSPITAL model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 取一条实体数据
        /// </summary>
        /// <param name="hosp_id">医院ID</param>
        /// <returns></returns>
        public HOSP_HOSPITAL GetOne(string hosp_id)
        {
            return DB.Load<HOSP_HOSPITAL, PK_HOSP_HOSPITAL>(new PK_HOSP_HOSPITAL { HOSP_ID = hosp_id });
        }

        public static HospDB Instance
        {
            get { return _Instance; }
        }
    }
}