﻿using System;
using System.Net;
using System.Net.Mail;

namespace WSS.BLL.Utils
{
    public class EmailSender
    {
        private readonly MailMessage _MailMessage;
        private readonly SmtpClient _SmtpClient;

        public EmailSender(String smtp_server, String user, String password)
        {
            _MailMessage = new MailMessage();
            _SmtpClient = new SmtpClient
                {
                    Host = smtp_server,
                    Port = 25,
                    Credentials = new NetworkCredential(user, password)
                };
        }

        public EmailSender(String smtp_server)
        {
            _MailMessage = new MailMessage();
            _SmtpClient = new SmtpClient(smtp_server);
        }

        public String SUBJECT
        {
            get
            {
                return _MailMessage.Subject;
            }
            set
            {
                _MailMessage.Subject = value;
            }
        }

        public String CONTENT
        {
            get
            {
                return _MailMessage.Body;
            }
            set
            {
                _MailMessage.Body = value;
            }
        }

        public String MAIL_FROM
        {
            get
            {
                return _MailMessage.From.Address;
            }
            set
            {
                _MailMessage.From = new MailAddress(value);
            }
        }

        public void AddReceiver(String email)
        {
            _MailMessage.To.Add(email);
        }

        public void Send()
        {
            _SmtpClient.Timeout = Int32.MaxValue;
            _SmtpClient.Send(_MailMessage);
        }

        public void AddAttachment(String filename)
        {
            _MailMessage.Attachments.Add(new Attachment(filename));
        }
    }
}