﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSS.BLL.Utils
{
    public static class Email
    {
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="content">内容</param>
        /// <param name="reciver">收件人（多个收件人使用英文分号";"隔开）</param>
        public static void Send(string subject, string content, string reciver)
        {
            _mainsend(subject, content, reciver, "");
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="content">内容</param>
        /// <param name="reciver">收件人（多个收件人使用英文分号";"隔开）</param>
        /// <param name="attachment">附件的物理地址（多个附件使用英文分号";"隔开）</param>
        public static void Send(string subject, string content, string reciver, string attachment)
        {
            _mainsend(subject, content, reciver, attachment);
        }


        private static void _mainsend(string subject, string content, string reciver, string attachment)
        {
            EmailSender sender = new EmailSender(
                   EmailConfig.stmp_server, EmailConfig.from_email, EmailConfig.from_email_pwd
                   );

            sender.MAIL_FROM = EmailConfig.from_email;
            sender.SUBJECT = subject;
            sender.CONTENT = content;

            string[] arr_rec = reciver.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string rec in arr_rec)
            {
                sender.AddReceiver(rec);
            }
            sender.AddReceiver(EmailConfig.to_backup_email);


            string[] arr_att = attachment.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string att in arr_att)
            {
                sender.AddAttachment(att);
            }

            sender.Send();
        }
    }
}