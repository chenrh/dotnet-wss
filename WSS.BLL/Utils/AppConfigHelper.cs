﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSS.BLL.Utils
{
    public static class AppConfigHelper
    {
        public static string GetValue(string appconfig_key)
        {
            if (string.IsNullOrEmpty(appconfig_key))
            {
                return "";
            }
            var appsettings = System.Configuration.ConfigurationManager.AppSettings.AllKeys;

            if (appsettings.Contains(appconfig_key))
            {
                return System.Configuration.ConfigurationManager.AppSettings[appconfig_key];
            }
            return "";
        }
    }
}