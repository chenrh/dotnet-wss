﻿using System.Collections.Generic;
using WSS.Models.Xt;
using WSS.Pub;

namespace WSS.BLL.Xt
{
    /// <summary>
    /// 功能表
    /// </summary>
    public class FunctionDb
    {
        private static readonly FunctionDb _Instance;

        private FunctionDb() { }

        static FunctionDb()
        {
            _Instance = new FunctionDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(XT_GNB model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(XT_GNB model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(XT_GNB model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }

        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="gnid">功能id</param>
        /// <returns></returns>
        public XT_GNB GetOne(string gnid)
        {
            return DB.Load<XT_GNB, PK_XT_GNB>(new PK_XT_GNB { GN_ID = gnid });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<XT_GNB> GetList(object param)
        {
            return DB.List<XT_GNB>(param.ToStrObjDict());
        }

        public static FunctionDb Instance
        {
            get { return _Instance; }
        }
    }
}