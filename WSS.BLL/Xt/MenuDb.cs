﻿
using System.Collections.Generic;
using WSS.Models.Res;
using WSS.Models.Xt;
using WSS.Pub;

namespace WSS.BLL.Xt
{
    /// <summary>
    /// 菜单
    /// </summary>
    public class MenuDb
    {
        private static readonly MenuDb _Instance;

        private MenuDb() { }

        static MenuDb()
        {
            _Instance = new MenuDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(XT_CDB model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(XT_CDB model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(XT_CDB model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }

        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="cdid">菜单ID</param>
        /// <returns></returns>
        public XT_CDB GetOne(string cdid)
        {
            return DB.Load<XT_CDB, PK_XT_CDB>(new PK_XT_CDB { CD_ID = cdid });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<XT_CDB> GetList(object param)
        {
            return DB.List<XT_CDB>(param.ToStrObjDict());
        }

        public static MenuDb Instance
        {
            get { return _Instance; }
        }
    }
}