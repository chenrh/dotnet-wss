﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSS.BLL.Resource
{
    /// <summary>
    /// 转化资源文件格式 时 需要使用的核心类
    /// </summary>
    public class BaseRes
    {
        /// <summary>
        /// PDF文件的保存物理路径
        /// </summary>
        public string PHYSICAL_PDF_FILE_PATH
        {
            get
            {
                if (MODEL_RES_RESOURCE.CONVERTED_FILE_NAME == "NONE")
                {
                    return MainHttpContext.Server.MapPath(MODEL_RES_RESOURCE.ORIG_PATH);
                }
                return MainHttpContext.Server.MapPath(MODEL_RES_RESOURCE.CONVERTED_FILE_NAME);
            }
        }

        /// <summary>
        /// 主程的 HttpContext
        /// </summary>
        public HttpContext MainHttpContext { get; set; }

        /// <summary>
        /// 上传资源的类别枚举
        /// </summary>
        public EResourceCategory ResourceCate { get; set; }

        /// <summary>
        /// 上传文件对应实体
        /// </summary>
        public WSS.Models.Res.RES_RESOURCE MODEL_RES_RESOURCE { get; set; }
    }
}