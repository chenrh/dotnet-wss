﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSS.BLL.Resource
{
    public class ResModel
    {

        public string Title { get; set; }

        public string Path { get; set; }

        public string Size { get; set; }

        public string Author { get; set; }

        public DateTime UploadDate { get; set; }

        public int HitCount { get; set; }

    }

    public enum EResourceCategory
    {
        Word,
        Pdf,
        Txt,
        Images,
        Vedio
    }
}