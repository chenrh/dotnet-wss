﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSS.Models.Res;
using WSS.Pub;

namespace WSS.BLL.Resource
{
    public class ResCenter
    {
        /// <summary>
        /// 得到grid中资源类别的formattet值
        /// </summary>
        /// <param name="datas"></param>
        /// <returns></returns>
        public string GetResCategoryGridFormatterValue(IList<RES_CATEGORY> datas)
        {
            var str = "{";
            foreach (RES_CATEGORY category in datas)
            {
                str += "'" + category.CATE_NO + "':'" + category.CATE_NAME + "',";
            }
            str = str.TrimEnd(',') + "}";
            return str;
        }
    }
}