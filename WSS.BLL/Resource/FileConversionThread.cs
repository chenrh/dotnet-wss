﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace WSS.BLL.Resource
{
    public class FileConversionThread
    {
        public bool IsConvertSuccess { get; private set; }
        public string Msg { get; private set; }

        public FileConversionThread(BaseRes base_res)
        {
            try
            {
                switch (base_res.ResourceCate)
                {
                    case EResourceCategory.Pdf:
                        PDF2SWF conver1 = new PDF2SWF(base_res);
                        break;
                    case EResourceCategory.Word:
                        DOC2PDF conver2 = new DOC2PDF(base_res);
                        break;
                }
                Msg = "转换成功";
                IsConvertSuccess = true;
            }
            catch (Exception ex)
            {
                Msg = ex.ToString();
                IsConvertSuccess = false;
            }
        }
    }

}