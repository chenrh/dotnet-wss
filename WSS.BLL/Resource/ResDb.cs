﻿using System.Collections;
using System.Collections.Generic;
using WSS.Models.Res;
using WSS.Pub;

namespace WSS.BLL.Resource
{
    /// <summary>
    /// 处理resource 与 数据库之间的关系
    /// </summary>
    public class ResDb
    {
        private static readonly ResDb _Instance;

        private ResDb() { }

        static ResDb()
        {
            _Instance = new ResDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(WSS.Models.Res.RES_RESOURCE model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(WSS.Models.Res.RES_RESOURCE model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="res_no">根据资源编号取资源数据</param>
        /// <returns></returns>
        public RES_RESOURCE GetOne(string res_no)
        {
            return DB.Load<RES_RESOURCE, PK_RES_RESOURCE>(new PK_RES_RESOURCE { RES_NO = res_no });
        }

        /// <summary>
        /// 返回列表
        /// </summary>
        /// <param name="param">参数(object,StrObjDict)</param>
        /// <returns></returns>
        public IList<RES_RESOURCE> GetList(object param)
        {
            var list = DB.List<RES_RESOURCE>(param.ToStrObjDict());
            return list;
        }


        public static ResDb Instance
        {
            get { return _Instance; }
        }
    }
}