﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace WSS.BLL.Resource
{
    public class PDF2SWF
    {
        /// <summary>
        /// Web.Config 中配置的 SWFTools 值
        /// </summary>
        private string _SWF_TOOLS_PATH { get; set; }

        /// <summary>
        /// Web.Config 中配置的 SWFTools_SaveTo 值
        /// </summary>
        private string _SWF_TOOLS_SAVE_TO_PATH { get; set; }

        private BaseRes _base_res { get; set; }

        public string SWF_RELATIVE_PATH { get; private set; }
        public string SWF_ABSOLUTE_PATH { get; private set; }

        public PDF2SWF(BaseRes base_res)
        {
            _base_res = base_res;

            _init();

            _convert(base_res.PHYSICAL_PDF_FILE_PATH);

        }

        private void _init()
        {
            var appsettings = System.Configuration.ConfigurationManager.AppSettings.AllKeys;
            if (appsettings.Contains("SWFTools"))
            {
                _SWF_TOOLS_PATH = System.Configuration.ConfigurationManager.AppSettings["SWFTools"];
            }
            else
            {
                throw new Exception("web.config 文件 appsetting 节点未配置 SWFTools ");
            }

            if (appsettings.Contains("SWFTools_SaveTo"))
            {
                _SWF_TOOLS_SAVE_TO_PATH = System.Configuration.ConfigurationManager.AppSettings["SWFTools_SaveTo"];

                if (!System.IO.Directory.Exists(_get_server_mappath(_SWF_TOOLS_SAVE_TO_PATH)))
                {
                    System.IO.Directory.CreateDirectory(_get_server_mappath(_SWF_TOOLS_SAVE_TO_PATH));
                }
            }
            else
            {
                throw new Exception("web.config 文件 appsetting 节点未配置 SWFTools_SaveTo ");
            }
        }

        private void _convert(string pdf_file_path)
        {
            /* try
             {*/
            SWF_RELATIVE_PATH = _base_res.MODEL_RES_RESOURCE.RES_NO + ".swf";
            SWF_ABSOLUTE_PATH = _get_server_mappath(_SWF_TOOLS_SAVE_TO_PATH + "\\" + SWF_RELATIVE_PATH);

            string exe = _SWF_TOOLS_PATH + @"\pdf2swf.exe";

            string args = pdf_file_path + " -o " + SWF_ABSOLUTE_PATH + "";

            using (System.Diagnostics.Process p = new System.Diagnostics.Process())
            {
                p.StartInfo.FileName = exe;
                p.StartInfo.Arguments = args;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = false;
                p.StartInfo.CreateNoWindow = true;
                p.Start();
                p.PriorityClass = System.Diagnostics.ProcessPriorityClass.Normal;
                p.WaitForExit();

                OnConvertOk();

            }

            /* }
             catch (Exception ex)
             {
                 throw new Exception("PDF文档转换SWF文件失败：" + ex.Message);
             }*/

        }


        private void OnConvertOk()
        {
            UPDATE_DB();
        }

        /// <summary>
        /// 更新数据库
        /// </summary>
        private void UPDATE_DB()
        {
            if (System.IO.File.Exists(SWF_ABSOLUTE_PATH))
            {
                FileInfo swf_info = new FileInfo(SWF_ABSOLUTE_PATH);
                _base_res.MODEL_RES_RESOURCE.RES_FILE_NAME = SWF_RELATIVE_PATH;
                _base_res.MODEL_RES_RESOURCE.RES_FILE_LEN = swf_info.Length.ToString();
                _base_res.MODEL_RES_RESOURCE.RES_PATH = _SWF_TOOLS_SAVE_TO_PATH + "/" + SWF_RELATIVE_PATH;
                ResDb.Instance.Update(_base_res.MODEL_RES_RESOURCE);
            }
            else
            {
                throw new Exception("文件转换失败！");
            }
        }

        private string _get_server_mappath(string path)
        {
            return _base_res.MainHttpContext.Server.MapPath(path);
        }
    }
}