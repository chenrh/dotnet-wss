﻿using System.Collections.Generic;
using WSS.Models.Res;
using WSS.Pub;

namespace WSS.BLL.Resource
{
    /// <summary>
    /// 资源类别
    /// </summary>
    public class ResCateDb
    {
        private static readonly ResCateDb _Instance;

        private ResCateDb() { }

        static ResCateDb()
        {
            _Instance = new ResCateDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(WSS.Models.Res.RES_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(WSS.Models.Res.RES_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(WSS.Models.Res.RES_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }

        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="cate_no">分类编号</param>
        /// <returns></returns>
        public RES_CATEGORY GetOne(string cate_no)
        {
            return DB.Load<RES_CATEGORY, PK_RES_CATEGORY>(new PK_RES_CATEGORY { CATE_NO = cate_no });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<Models.Res.RES_CATEGORY> GetList(object param)
        {
            return DB.List<Models.Res.RES_CATEGORY>(param.ToStrObjDict());
        }

        public static ResCateDb Instance
        {
            get { return _Instance; }
        }
    }
}