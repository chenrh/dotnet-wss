﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSS.Pub;
using WSS.Models.Test;

namespace WSS.BLL.Test
{
    /// <summary>
    /// 试题答案
    /// </summary>
    public class QAnswerDb
    {
        private static readonly QAnswerDb _Instance;

        private QAnswerDb() { }

        static QAnswerDb()
        {
            _Instance = new QAnswerDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(TEST_ANSWER model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(TEST_ANSWER model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(TEST_ANSWER model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }
        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="answer_id">answer_id</param>
        /// <returns></returns>
        public TEST_ANSWER GetOne(string answer_id)
        {
            return DB.Load<TEST_ANSWER, PK_TEST_ANSWER>(new PK_TEST_ANSWER { ANSWER_ID = answer_id });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<TEST_ANSWER> GetList(object param)
        {
            return DB.List<TEST_ANSWER>(param.ToStrObjDict());
        }

        public static QAnswerDb Instance
        {
            get { return _Instance; }
        }
    }
}