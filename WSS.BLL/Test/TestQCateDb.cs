﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSS.Pub;
using WSS.Models.Test;

namespace WSS.BLL.Test
{
    /// <summary>
    /// 试题类别
    /// </summary>
    public class TestQCateDb
    {
        private static readonly TestQCateDb _Instance;

        private TestQCateDb() { }

        static TestQCateDb()
        {
            _Instance = new TestQCateDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(TEST_QUESTION_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(TEST_QUESTION_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(TEST_QUESTION_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }

        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="cate_no">cate_no</param>
        /// <returns></returns>
        public TEST_QUESTION_CATEGORY GetOne(string cate_no)
        {
            return DB.Load<TEST_QUESTION_CATEGORY, PK_TEST_QUESTION_CATEGORY>(new PK_TEST_QUESTION_CATEGORY { CATE_NO = cate_no });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<TEST_QUESTION_CATEGORY> GetList(object param)
        {
            return DB.List<TEST_QUESTION_CATEGORY>(param.ToStrObjDict());
        }

        public static TestQCateDb Instance
        {
            get { return _Instance; }
        }
    }
}