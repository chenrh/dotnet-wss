﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSS.Pub;
using WSS.Models.Test;

namespace WSS.BLL.Test
{
    /// <summary>
    /// 试卷
    /// </summary>
    public class TestDb
    {
        private static readonly TestDb _Instance;

        private TestDb() { }

        static TestDb()
        {
            _Instance = new TestDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(TEST_TEST model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(TEST_TEST model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(TEST_TEST model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }
        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="test_id">test_id</param>
        /// <returns></returns>
        public TEST_TEST GetOne(string test_id)
        {
            return DB.Load<TEST_TEST, PK_TEST_TEST>(new PK_TEST_TEST { TEST_ID = test_id });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<TEST_TEST> GetList(object param)
        {
            return DB.List<TEST_TEST>(param.ToStrObjDict());
        }

        public static TestDb Instance
        {
            get { return _Instance; }
        }
    }
}