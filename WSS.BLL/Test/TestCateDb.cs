﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSS.Pub;
using WSS.Models.Test;

namespace WSS.BLL.Test
{
    /// <summary>
    /// 试卷类别
    /// </summary>
    public class TestCateDb
    {
        private static readonly TestCateDb _Instance;

        private TestCateDb() { }

        static TestCateDb()
        {
            _Instance = new TestCateDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(TEST_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(TEST_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(TEST_CATEGORY model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }

        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="cate_no">分类编号</param>
        /// <returns></returns>
        public TEST_CATEGORY GetOne(string cate_no)
        {
            return DB.Load<TEST_CATEGORY, PK_TEST_CATEGORY>(new PK_TEST_CATEGORY { CATE_NO = cate_no });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<TEST_CATEGORY> GetList(object param)
        {
            return DB.List<TEST_CATEGORY>(param.ToStrObjDict());
        }

        public static TestCateDb Instance
        {
            get { return _Instance; }
        }
    }
}