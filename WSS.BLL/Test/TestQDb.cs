﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSS.Pub;
using WSS.Models.Test;

namespace WSS.BLL.Test
{
    /// <summary>
    /// 试题
    /// </summary>
    public class TestQDb
    {
        private static readonly TestQDb _Instance;

        private TestQDb() { }

        static TestQDb()
        {
            _Instance = new TestQDb();
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(TEST_QUESTION model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(TEST_QUESTION model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="model"></param>
        public void Delete(TEST_QUESTION model)
        {
            DBState db = new DBState { Name = model.MAP_DELETE, Param = model, Type = ESqlType.DELETE };
            DB.Execute(db);
        }

        /// <summary>
        /// 得到实体
        /// </summary>
        /// <param name="question_id">question_id</param>
        /// <returns></returns>
        public TEST_QUESTION GetOne(string question_id)
        {
            return DB.Load<TEST_QUESTION, PK_TEST_QUESTION>(new PK_TEST_QUESTION { QUESTION_ID = question_id });
        }

        /// <summary>
        /// 得到集合
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IList<TEST_QUESTION> GetList(object param)
        {
            return DB.List<TEST_QUESTION>(param.ToStrObjDict());
        }

        public static TestQDb Instance
        {
            get { return _Instance; }
        }
    }
}