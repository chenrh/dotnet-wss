﻿using System;

namespace WSS.BLL.Account
{
    public class Account : IAccount
    {
        public Account(string guid)
        {
            Guid val;
            MyGuid = Guid.TryParse(guid, out val) ? val : new Guid();
        }

        private Guid MyGuid { get; set; }
       
        public UserOnlineModel CurrentUserInfo
        {
            get
            {
                return UserOnline.GetOnlineUserByGuid(MyGuid);
            }
        }
        public string UserId
        {
            get { return CurrentUserInfo.UserId; }
        }
        public string UserName
        {
            get { return CurrentUserInfo.UserName; }
        }
   
        public bool IsLogon
        {
            get { return CurrentUserInfo != null; }
        }
        public string ActionPath
        {
            get
            {
                return CurrentUserInfo.ActionPath;
            }
        }
        public DateTime ActionTime
        {
            get
            {
                return CurrentUserInfo.ActionTime;
            }
        }
        public DateTime LoginTime
        {
            get
            {
                return CurrentUserInfo.LoginTime;
            }
        }
    }
}