﻿using System;
using System.Collections.Generic;
using WSS.Models.User;

namespace WSS.BLL.Account
{
    /// <summary>
    /// 在线缓存数据操作类
    /// </summary>
    public class OnlineHelper
    {
        private Guid _guid;

        /// <summary>
        /// 返回用户在线信息的集合
        /// </summary>
        /// <returns></returns>
        public List<UserOnlineModel> GetOnline()
        {
            return UserOnline.OnlineList;
        }

        /// <summary>
        /// 初始登录时缓存初始数据
        /// </summary>
        public Guid CacheUserData(USER_USER user)
        {
            //模拟数据
            var model = new UserOnlineModel
            {
                Guid = Guid.NewGuid(),
                LoginTime = DateTime.Now,
                UserId = user.USER_ID,
                UserName = user.USER_NAME,
                ActionTime = DateTime.Now,
                ActionPath = "/Login/Login",
                UserInfo = user
            };

            UserOnline.AddOnlineUser(model);

            return model.Guid;
        }

        /// <summary>
        /// 授权检查时，如果登录数据丢失则再次缓存
        /// </summary>
        public void CacheUserData(USER_USER user, string guid, string actionurl)
        {
            if (Guid.TryParse(guid, out _guid))
            {
                var model = new UserOnlineModel
                {
                    Guid = _guid,
                    LoginTime = DateTime.Now,
                    UserId = user.USER_ID,
                    UserName = user.USER_NAME,
                    ActionTime = DateTime.Now,
                    ActionPath = actionurl,
                    UserInfo = user
                };

                UserOnline.AddOnlineUser(model);
            }
        }


        /// <summary>
        /// 更新最后操作时间和最后操作路径
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="path"></param>
        public void UpdateUserLastAction(string guid, string path)
        {
            if (Guid.TryParse(guid, out _guid))
            {
                IAccount account = new Account(guid);
                if (account.IsLogon)
                {
                    account.CurrentUserInfo.ActionTime = DateTime.Now;
                    account.CurrentUserInfo.ActionPath = path.Replace(guid, "*");
                    UserOnline.UpdateOnlineUser(_guid, account.CurrentUserInfo);
                }
            }
        }
 
        /// <summary>
        /// 根据guid删除缓存数据
        /// </summary>
        /// <param name="guid"></param>
        public void RemoveUserInfoByGuid(string guid)
        {
            if (Guid.TryParse(guid, out _guid))
            {
                UserOnline.RemoveOnlineUser(_guid);
            }
        }

        /// <summary>
        /// 通过guid查找缓存数据
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public UserOnlineModel GetUserInfoByGuid(string guid)
        {
            if (Guid.TryParse(guid, out _guid))
            {
                return UserOnline.GetOnlineUserByGuid(_guid);
            }
            return null;
        }

        /// <summary>
        /// 通过UserId查找缓存数据
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public UserOnlineModel GetUserInfoByUserId(string userid)
        {
            return UserOnline.GetOnlineUserByUserId(userid);
        }

        /// <summary>
        /// 更新用户缓存
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="usermodel"></param>
        public void UpdateUserInfo(string guid, USER_USER usermodel)
        {
            if (Guid.TryParse(guid, out _guid))
            {
                UserOnlineModel uom = UserOnline.GetOnlineUserByGuid(_guid);
                if (uom != null)
                {
                    uom.ActionPath = "/unknown/updateusercache";
                    uom.ActionTime = DateTime.Now;
                    uom.UserInfo = usermodel;
                    uom.UserName = usermodel.USER_NAME;
                    UserOnline.UpdateOnlineUser(_guid, uom);
                }
            }
        }
    }
}