﻿using System;

namespace WSS.BLL.Account
{
    /// <summary>
    /// 登录用户账号信息接口
    /// </summary>
    public interface IAccount
    {
        string UserId { get; } 
        string UserName { get; }
        bool IsLogon { get; }
        string ActionPath { get; }
        DateTime ActionTime { get; }
        DateTime LoginTime { get; }
        UserOnlineModel CurrentUserInfo { get; }
    }
}