﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace WSS.BLL.Account
{
    public class UserOnlineModel
    {
        public Guid Guid { get; set; }
         
        public string UserId { get; set; }
        public string UserName { get; set; }

        public DateTime LoginTime { get; set; }

        public DateTime ActionTime { get; set; }

        public string ActionPath { get; set; }

        public WSS.Models.User.USER_USER UserInfo { get; set; }

    }
}