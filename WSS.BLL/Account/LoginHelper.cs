﻿
using System;
using System.Linq;
using WSS.BLL.Utils;
using WSS.Models.User;
using WSS.Pub;

namespace WSS.BLL.Account
{
    public class LoginHelper
    {

        public bool Login(ref string msg, string username, string userpwd, string verificode, string hospitalid)
        {
            if (string.IsNullOrEmpty(username))
            {
                msg = "帐号不可为空";
                return false;
            }

            if (string.IsNullOrEmpty(hospitalid))
            {
                msg = "帐号所属医院不可为空";
                return false;
            }

            if (!_CheckVericode(verificode, ref msg))
            {
                return false;
            }

            var list = DB.List<USER_USER>(new
            {
                HOSP_ID = hospitalid,
                USER_NO = username
            }.ToStrObjDict());

            USER_USER model = new USER_USER();

            if (list != null && list.Count == 1)
            {
                model = list.First();
            }

            if (string.IsNullOrEmpty(model.USER_ID))
            {
                msg = "帐号不存在";
                return false;
            }

            string encodepwd = MD5.MD5Encrypt(userpwd, 32);
            if (model.USER_PWD != encodepwd)
            {
                msg = "密码不匹配";
                return false;
            }

            if (model.FLAG_LOGIN != 1)
            {
                msg = "您被限制登录";
                return false;
            }


            Guid guid = new OnlineHelper().CacheUserData(model);

            msg = guid + "," + model.USER_ID + "," + model.USER_NO + "," + model.HOSP_ID;

            return true;
        }

        public bool UnLockScreen(ref string msg, string userid, string userpwd, string hospitalid, string guid)
        {
            if (string.IsNullOrEmpty(userid))
            {
                msg = "帐号不可为空";
                return false;
            }

            if (string.IsNullOrEmpty(hospitalid))
            {
                msg = "帐号所属医院不可为空";
                return false;
            }

            var list = DB.List<USER_USER>(new { HOSP_ID = hospitalid, USER_ID = userid }.ToStrObjDict());

            USER_USER model = new USER_USER();

            if (list != null && list.Count == 1)
            {
                model = list.First();
            }

            if (string.IsNullOrEmpty(model.USER_ID))
            {
                msg = "帐号不存在";
                return false;
            }

            string encodepwd = MD5.MD5Encrypt(userpwd, 32);
            if (model.USER_PWD != encodepwd)
            {
                msg = "密码不匹配";
                return false;
            }

            if (model.FLAG_LOGIN != 1)
            {
                msg = "您被限制登录";
                return false;
            }

            Guid cookieguid;
            if (!Guid.TryParse(guid, out cookieguid))
            {
                msg = "解锁失败，请重新登录";
                return false;
            }
            var useronlinemodel = new UserOnlineModel
            {
                Guid = cookieguid,
                LoginTime = DateTime.Now,
                UserId = model.USER_ID,
                UserName = model.USER_NAME,
                ActionTime = DateTime.Now,
                ActionPath = "/Login/UnLockScreen",
                UserInfo = model
            };

            UserOnline.AddOnlineUser(useronlinemodel);
            msg = "";

            return true;
        }

        private bool _CheckVericode(string code, ref string msg)
        {
            var verificode = System.Web.HttpContext.Current.Session["VerificationCode"] as string;

            if (verificode != "%-not-need-%")
            {
                return true;
            }


            if (string.IsNullOrEmpty(verificode))
            {
                msg = "验证码过期，请重试";
                return false;
            }

            if (string.IsNullOrEmpty(code))
            {
                msg = "请输入验证码";
                return false;
            }

            if (verificode.ToUpper() != code.ToUpper())
            {
                msg = "验证码不匹配";
                return false;
            }

            return true;
        }


        /// <summary>
        /// 用户登出
        ///  1、清除其在系统中的缓存信息
        ///  2、清除其所有会话信息
        /// </summary>
        /// <param name="guid"></param>
        public void Logout(string guid)
        {
            var online_helper = new OnlineHelper();
            online_helper.RemoveUserInfoByGuid(guid);
            System.Web.HttpContext.Current.Session.Abandon();
        }
    }
}