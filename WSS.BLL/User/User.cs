﻿using System;
using System.Collections.Generic;
using WSS.Models.User;
using WSS.Pub;

namespace WSS.BLL.User
{
    public class User
    {
        /// <summary>
        /// 职工所属医院
        /// </summary>
        private string HOSPITAL_ID { get; set; }

        public User(string hospital_id)
        {
            HOSPITAL_ID = hospital_id;
        }

        /// <summary>
        /// 写数据库
        /// </summary>
        /// <param name="model"></param>
        public void Insert(USER_USER model)
        {
            DBState db = new DBState { Name = model.MAP_INSERT, Param = model, Type = ESqlType.INSERT };
            DB.Execute(db);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        public void Update(USER_USER model)
        {
            DBState db = new DBState { Name = model.MAP_UPDATE, Param = model, Type = ESqlType.UPDATE };
            DB.Execute(db);
        }

        /// <summary>
        /// 得到一个用户实例
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="userno"></param>
        /// <returns></returns>
        public USER_USER GetOne(string userid, string userno)
        {
            return DB.Load<USER_USER, PK_USER_USER>(new PK_USER_USER
            {
                HOSP_ID = HOSPITAL_ID,
                USER_ID = userid,
                USER_NO = userno
            });
        }

        /// <summary>
        /// 用户列表
        /// </summary>
        /// <returns></returns>
        public IList<USER_USER> GetList(StrObjDict param)
        {
            if (!param.ContainsKey("HOSP_ID"))
            {
                param.Merger(new { HOSP_ID = HOSPITAL_ID });
            }
            var list = DB.List<USER_USER>(param);

            for (int i = 0; i < list.Count; i++)
            {
                list[i].USER_PWD = "******";
            }

            return list;
        }
    }
}