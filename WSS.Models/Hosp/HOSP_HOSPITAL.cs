﻿/*
* 陈日红@2014-04-23 11:29:31 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.Hosp
{
    /// <summary>
    /// HOSP_HOSPITAL模型
    /// </summary>
    public class HOSP_HOSPITAL : BaseModel
    {
        #region 属性
        /// <summary>
        /// 医院名称
        /// </summary>
        public string HOSP_NAME { get; set; }
        /// <summary>
        /// 医院ID
        /// </summary>
        public string HOSP_ID { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "HOSP_HOSPITAL";
        }
        public override string GetTableName()
        {
            return "HOSP_HOSPITAL";
        }
    }

    /// <summary>
    /// HOSP_HOSPITAL主键
    /// </summary>
    public class PK_HOSP_HOSPITAL
    {
        #region 属性
        /// <summary>
        /// 医院ID
        /// </summary>
        public string HOSP_ID { get; set; }
        #endregion
    }
}
