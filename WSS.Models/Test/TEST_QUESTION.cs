﻿/*
* 张沛@2014-05-20 11:55:53
*/
using System;
using WSS.Pub;
namespace WSS.Models.Test
{
    /// <summary>
    /// TEST_QUESTION模型
    /// </summary>
    public class TEST_QUESTION : BaseModel
    {
        #region 属性
        /// <summary>
        /// 问题ID
        /// </summary>
        public string QUESTION_ID { get; set; }
        /// <summary>
        /// 所属试题ID
        /// </summary>
        public string TEST_ID { get; set; }
        /// <summary>
        /// 问题序号
        /// </summary>
        public string QUESTION_NO { get; set; }
        /// <summary>
        /// 问题类别
        /// </summary>
        public string CATE_NO { get; set; }
        /// <summary>
        /// 问题类型 1单选2多选3对错
        /// </summary>
        public int? QUESTION_TYPE { get; set; }
        /// <summary>
        /// 问题标题
        /// </summary>
        public string QUESTION_TITLE { get; set; }
        /// <summary>
        /// 问题详情
        /// </summary>
        public string QUESTION_CONTENTS { get; set; }
        /// <summary>
        /// 问题解析
        /// </summary>
        public string QUESTION_ANALYSIS { get; set; }
        /// <summary>
        /// 作废判别
        /// </summary>
        public int? FLAG_INVALID { get; set; }
        /// <summary>
        /// 问题难度等级
        /// </summary>
        public int? QUESTION_LEVEL { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "TEST_QUESTION";
        }
        public override string GetTableName()
        {
            return "TEST_QUESTION";
        }
    }

    /// <summary>
    /// TEST_QUESTION主键
    /// </summary>
    public class PK_TEST_QUESTION
    {
        #region 属性
        /// <summary>
        /// 问题ID
        /// </summary>
        public string QUESTION_ID { get; set; }
        #endregion
    }

}
