﻿/*
* 张沛@2014-05-21 13:35:29
*/
using System;
using WSS.Pub;
namespace WSS.Models.Test
{
    /// <summary>
    /// TEST_TEST模型
    /// </summary>
    public class TEST_TEST : BaseModel
    {
        #region 属性
        /// <summary>
        /// 试题ID
        /// </summary>
        public string TEST_ID { get; set; }
        /// <summary>
        /// 分类编号
        /// </summary>
        public string CATE_NO { get; set; }
        /// <summary>
        /// 试题名称
        /// </summary>
        public string TEST_NAME { get; set; }
        /// <summary>
        /// 试题描述
        /// </summary>
        public string TEST_CONTENTS { get; set; }
        /// <summary>
        /// 试题年份
        /// </summary>
        public string TEST_YEAR { get; set; }
        /// <summary>
        /// 试题难度
        /// </summary>
        public string TEST_LEVEL { get; set; }
        /// <summary>
        /// 上传者ID
        /// </summary>
        public string UPLOAD_USERID { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? UPLOAD_TIME { get; set; }
        /// <summary>
        /// 浏览数
        /// </summary>
        public long? READCOUNT { get; set; }
        /// <summary>
        /// 作废判别
        /// </summary>
        public int? FLAG_INVALID { get; set; }
        /// <summary>
        /// 排序编号
        /// </summary>
        public string ORDER_NO { get; set; }
        /// <summary>
        /// 医院ID
        /// </summary>
        public string HOSP_ID { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "TEST_TEST";
        }
        public override string GetTableName()
        {
            return "TEST_TEST";
        }
    }

    /// <summary>
    /// TEST_TEST主键
    /// </summary>
    public class PK_TEST_TEST
    {
        #region 属性
        /// <summary>
        /// 试题ID
        /// </summary>
        public string TEST_ID { get; set; }
        #endregion
    }
}
