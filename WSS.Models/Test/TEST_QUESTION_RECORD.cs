﻿/*
* 张沛@2014-05-20 11:59:31
*/
using System;
using WSS.Pub;
namespace WSS.Models.Test
{
    /// <summary>
    /// TEST_QUESTION_RECORD模型
    /// </summary>
    public class TEST_QUESTION_RECORD : BaseModel
    {
        #region 属性
        /// <summary>
        /// 试题ID
        /// </summary>
        public string TEST_ID { get; set; }
        /// <summary>
        /// 问题ID
        /// </summary>
        public string QUESTION_ID { get; set; }
        /// <summary>
        /// 作答用户ID
        /// </summary>
        public string USER_ID { get; set; }
        /// <summary>
        /// 问题答案
        /// </summary>
        public string ANSWER { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "TEST_QUESTION_RECORD";
        }
        public override string GetTableName()
        {
            return "TEST_QUESTION_RECORD";
        }
    }

    /// <summary>
    /// TEST_QUESTION_RECORD主键
    /// </summary>
    public class PK_TEST_QUESTION_RECORD
    {
        #region 属性
        /// <summary>
        /// 试题ID
        /// </summary>
        public string TEST_ID { get; set; }
        /// <summary>
        /// 问题ID
        /// </summary>
        public string QUESTION_ID { get; set; }
        /// <summary>
        /// 作答用户ID
        /// </summary>
        public string USER_ID { get; set; }
        #endregion
    }
}
