﻿/*
* 张沛@2014-05-20 11:58:53
*/
using System;
using WSS.Pub;
namespace WSS.Models.Test
{
    /// <summary>
    /// TEST_QUESTION_CATEGORY模型
    /// </summary>
    public class TEST_QUESTION_CATEGORY : BaseModel
    {
        #region 属性
        /// <summary>
        /// 分类编号
        /// </summary>
        public string CATE_NO { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string CATE_NAME { get; set; }
        /// <summary>
        /// 排序编号
        /// </summary>
        public int? ORDER_NO { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "TEST_QUESTION_CATEGORY";
        }
        public override string GetTableName()
        {
            return "TEST_QUESTION_CATEGORY";
        }
    }

    /// <summary>
    /// TEST_QUESTION_CATEGORY主键
    /// </summary>
    public class PK_TEST_QUESTION_CATEGORY
    {
        #region 属性
        /// <summary>
        /// 分类编号
        /// </summary>
        public string CATE_NO { get; set; }
        #endregion
    }
}
