﻿/*
* 张沛@2014-05-20 11:55:15
*/
using System;
using WSS.Pub;
namespace WSS.Models.Test
{
    /// <summary>
    /// TEST_ANSWER模型
    /// </summary>
    public class TEST_ANSWER : BaseModel
    {
        #region 属性
        /// <summary>
        /// 答案ID
        /// </summary>
        public string ANSWER_ID { get; set; }
        /// <summary>
        /// 所属问题ID
        /// </summary>
        public string QUESTION_ID { get; set; }
        /// <summary>
        /// 答案序号
        /// </summary>
        public string ANSWER_NO { get; set; }
        /// <summary>
        /// 答案内容
        /// </summary>
        public string CONTENTS { get; set; }
        /// <summary>
        /// 答案描述
        /// </summary>
        public string DETAILS { get; set; }
        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public int? FLAG_ISRIGHT { get; set; }
        /// <summary>
        /// 作废判别
        /// </summary>
        public int? FLAG_INVALID { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "TEST_ANSWER";
        }
        public override string GetTableName()
        {
            return "TEST_ANSWER";
        }
    }

    /// <summary>
    /// TEST_ANSWER主键
    /// </summary>
    public class PK_TEST_ANSWER
    {
        #region 属性
        /// <summary>
        /// 答案ID
        /// </summary>
        public string ANSWER_ID { get; set; }
        #endregion
    }
}
