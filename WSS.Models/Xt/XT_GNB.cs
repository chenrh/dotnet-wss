﻿/*
* 陈日红@2014-05-21 11:35:48 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.Xt
{
    /// <summary>
    /// XT_GNB模型
    /// </summary>
    public class XT_GNB : BaseModel
    {
        #region 属性
        /// <summary>
        /// 功能ID
        /// </summary>
        public string GN_ID { get; set; }
        /// <summary>
        /// 功能名称
        /// </summary>
        public string GN_MC { get; set; }
        /// <summary>
        /// Controller名称
        /// </summary>
        public string GN_CONTROLLER { get; set; }
        /// <summary>
        /// Action名称
        /// </summary>
        public string GN_ACTION { get; set; }
        /// <summary>
        /// 简写码
        /// </summary>
        public string JXM { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "XT_GNB";
        }
        public override string GetTableName()
        {
            return "XT_GNB";
        }
    }

    /// <summary>
    /// XT_GNB主键
    /// </summary>
    public class PK_XT_GNB
    {
        #region 属性
        /// <summary>
        /// 功能ID
        /// </summary>
        public string GN_ID { get; set; }
        #endregion
    }
}
