﻿/*
* 陈日红@2014-05-21 11:31:41 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.Xt
{
    /// <summary>
    /// XT_CDB模型
    /// </summary>
    public class XT_CDB : BaseModel
    {
        #region 属性
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string CD_ID { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string CD_MC { get; set; }
        /// <summary>
        /// 功能ID
        /// </summary>
        public string CD_GNID { get; set; }
        /// <summary>
        /// 菜单参数
        /// </summary>
        public string CD_CS { get; set; }
        /// <summary>
        /// 简写码
        /// </summary>
        public string JXM { get; set; }
        /// <summary>
        /// 作废判别
        /// </summary>
        public int? FLAG_INVALID { get; set; }
        /// <summary>
        /// 上级菜单
        /// </summary>
        public string CD_PID { get; set; }
        /// <summary>
        /// 层级
        /// </summary>
        public int? CD_LEVEL { get; set; }
        /// <summary>
        /// 末级判别
        /// </summary>
        public int? FLAG_LAST { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public long? ORDER_NO { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string CD_ICON { get; set; }
        /// <summary>
        /// 背景色
        /// </summary>
        public string CD_BKCOLOR { get; set; }
        /// <summary>
        /// 字体
        /// </summary>
        public string CD_FONT { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "XT_CDB";
        }
        public override string GetTableName()
        {
            return "XT_CDB";
        }
    }

    /// <summary>
    /// XT_CDB主键
    /// </summary>
    public class PK_XT_CDB
    {
        #region 属性
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string CD_ID { get; set; }
        #endregion
    }
}
