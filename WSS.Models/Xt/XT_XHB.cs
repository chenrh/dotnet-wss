﻿/*
* 陈日红@2014-04-25 17:41:26 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.Xt
{
    /// <summary>
    /// XT_XHB模型
    /// </summary>
    public class XT_XHB : BaseModel
    {
        #region 属性
        /// <summary>
        /// 代码
        /// </summary>
        public string BM { get; set; }
        /// <summary>
        /// 当前值
        /// </summary>
        public long? DQZ { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "XT_XHB";
        }
        public override string GetTableName()
        {
            return "XT_XHB";
        }
    }

    /// <summary>
    /// XT_XHB主键
    /// </summary>
    public class PK_XT_XHB
    {
        #region 属性
        /// <summary>
        /// 代码
        /// </summary>
        public string BM { get; set; }
        #endregion
    }
}
