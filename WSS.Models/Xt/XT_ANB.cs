﻿/*
* 陈日红@2014-05-21 11:31:05 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.Xt
{
    /// <summary>
    /// XT_ANB模型
    /// </summary>
    public class XT_ANB : BaseModel
    {
        #region 属性
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string CD_ID { get; set; }
        /// <summary>
        /// 按钮名称
        /// </summary>
        public string AN_ELEMENT_TEXT { get; set; }
        /// <summary>
        /// 按钮ID
        /// </summary>
        public string AN_ELEMENT_ID { get; set; }
        /// <summary>
        /// 按钮样式
        /// </summary>
        public string AN_ELEMNET_CLASS { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public long? ORDER_NO { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "XT_ANB";
        }
        public override string GetTableName()
        {
            return "XT_ANB";
        }
    }

    /// <summary>
    /// XT_ANB主键
    /// </summary>
    public class PK_XT_ANB
    {
        #region 属性
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string CD_ID { get; set; }
        /// <summary>
        /// 按钮ID
        /// </summary>
        public string AN_ELEMENT_ID { get; set; }
        #endregion
    }
}
