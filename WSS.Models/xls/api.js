﻿//这是一段伪代码
var mainfn = {};
function callback() {
    // 回调
}
//=================================================

// 弹窗
//参数 标题
//参数 内容
//参数 回调
//参数 按钮文本（默认为"保存"）

$("#btn_cate").click(function () {
    mainfn.dialog("abc", "dd", callback, "提交");
});

// 数据表格 datagrid
// 格式时间方式 formatter: { type: "date", fmt: "yyyy-MM-dd hh:mm" } // 需要使用到main.js内的方法

$("#maingrid").bootgrid({
    url: "/rescenter/list",
    pagesize: 5,
    pager: "pagebar",
    headers: ["#", "资源名称", "资源大小", "上传人", "上传时间", "阅读次数", "下载次数", "操作"],
    cols: [
        { id: "RES_NO", align: "center" },
        { id: "ORIG_FILE_NAME", align: "left" },
        { id: "ORIG_FILE_LEN", align: "center" },
        { id: "UPLOADER", align: "center" },
        { id: "UPLOAD_TIME", align: "center", formatter: { type: "date", fmt: "yyyy-MM-dd hh:mm" } },
        { id: "TOTAL_READ", align: "center" },
        { id: "TOTAL_DOWNLOAD", align: "center" },
        { id: "RES_NO", align: "center" }
    ]
});

// { id: 'USER_GENDER', formatter: { type: "select", fmt: { "1": "男", "2": "女" } } },
// { id: "ORIG_FILE_NAME", align: "left", formatter: { type: "link", fmt: { href: "<a href='/rescenter/details/{1}'>{0}</a>", val: "ORIG_FILE_NAME,RES_NO" } } },

$("#btn_sch").click(function () {
    $("#maingrid").bootgrid("setGridParam", { resname: $("#sch_resname").val() });
    $("#maingrid").bootgrid("reloadGrid", { page: 1 });
});
