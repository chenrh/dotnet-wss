﻿/*
* 陈日红@2014-05-23 16:07:57 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.User
{
    /// <summary>
    /// USER_GROUP模型
    /// </summary>
    public class USER_GROUP : BaseModel
    {
        #region 属性
        /// <summary>
        /// 用户组Id
        /// </summary>
        public string USER_GROUP_ID { get; set; }
        /// <summary>
        /// 医院ID
        /// </summary>
        public string HOSP_ID { get; set; }
        /// <summary>
        /// 用户组名称
        /// </summary>
        public string USER_GROUP_NAME { get; set; }
        /// <summary>
        /// 是否作废
        /// </summary>
        public int? FLAG_INVALID { get; set; }
        /// <summary>
        /// 排序编号
        /// </summary>
        public int? ORDER_NO { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "USER_GROUP";
        }
        public override string GetTableName()
        {
            return "USER_GROUP";
        }
    }

    /// <summary>
    /// USER_GROUP主键
    /// </summary>
    public class PK_USER_GROUP
    {
        #region 属性
        /// <summary>
        /// 用户组Id
        /// </summary>
        public string USER_GROUP_ID { get; set; }
        #endregion
    }
}
