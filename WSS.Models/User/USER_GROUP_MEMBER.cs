﻿/*
* 陈日红@2014-05-23 16:10:46 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.User
{
    /// <summary>
    /// USER_GROUP_MEMBER模型
    /// </summary>
    public class USER_GROUP_MEMBER : BaseModel
    {
        #region 属性
        /// <summary>
        /// 用户组Id
        /// </summary>
        public string USER_GROUP_ID { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string USER_ID { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "USER_GROUP_MEMBER";
        }
        public override string GetTableName()
        {
            return "USER_GROUP_MEMBER";
        }
    }

    /// <summary>
    /// USER_GROUP_MEMBER主键
    /// </summary>
    public class PK_USER_GROUP_MEMBER
    {
        #region 属性
        /// <summary>
        /// 用户组Id
        /// </summary>
        public string USER_GROUP_ID { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string USER_ID { get; set; }
        #endregion
    }
}
