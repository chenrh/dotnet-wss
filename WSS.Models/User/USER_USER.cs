﻿/*
* 陈日红@2014-04-23 09:46:30 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.User
{
    /// <summary>
    /// USER_USER模型
    /// </summary>
    public class USER_USER : BaseModel
    {
        #region 属性
        /// <summary>
        /// 用户ID
        /// </summary>
        public string USER_ID { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string USER_NAME { get; set; }
        /// <summary>
        /// QQ号码
        /// </summary>
        public string USER_QQ { get; set; }
        /// <summary>
        /// 用户工号
        /// </summary>
        public string USER_NO { get; set; }
        /// <summary>
        /// 用户密码
        /// </summary>
        public string USER_PWD { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public int? USER_GENDER { get; set; }
        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string USER_EMAIL { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string USER_PHONE { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string USER_IDCARD { get; set; }
        /// <summary>
        /// 授权登录
        /// </summary>
        public int? FLAG_LOGIN { get; set; }
        /// <summary>
        /// 邮箱验证
        /// </summary>
        public int? FLAG_MAIL { get; set; }
        /// <summary>
        /// 手机验证
        /// </summary>
        public int? FLAG_PHONE { get; set; }
        /// <summary>
        /// 医院ID
        /// </summary>
        public string HOSP_ID { get; set; }
        /// <summary>
        /// 用户头像
        /// </summary>
        public string USER_PIC { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "USER_USER";
        }
        public override string GetTableName()
        {
            return "USER_USER";
        }
    }

    /// <summary>
    /// USER_USER主键
    /// </summary>
    public class PK_USER_USER
    {
        #region 属性
        /// <summary>
        /// 用户ID
        /// </summary>
        public string USER_ID { get; set; }
        /// <summary>
        /// 用户工号
        /// </summary>
        public string USER_NO { get; set; }
        /// <summary>
        /// 医院ID
        /// </summary>
        public string HOSP_ID { get; set; }
        #endregion
    }
}
