﻿/*
* 陈日红@2014-04-18 10:58:22 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.Res
{
    /// <summary>
    /// RES_RESOURCE模型
    /// </summary>
    public class RES_RESOURCE : BaseModel
    {
        #region 属性
        /// <summary>
        /// 资源编号
        /// </summary>
        public string RES_NO { get; set; }
        /// <summary>
        /// 资源名称
        /// </summary>
        public string RES_NAME { get; set; }
        /// <summary>
        /// 资源分类编号
        /// </summary>
        public string CATE_NO { get; set; }
        /// <summary>
        /// 资源排序编号
        /// </summary>
        public int ORDER_NO { get; set; }
        /// <summary>
        /// 上传者
        /// </summary>
        public string UPLOADER { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? UPLOAD_TIME { get; set; }
        /// <summary>
        /// 原始文件相对对路径
        /// </summary>
        public string ORIG_PATH { get; set; }
        /// <summary>
        /// 现文件相对路径
        /// </summary>
        public string RES_PATH { get; set; }
        /// <summary>
        /// 原始文件名（文件名.扩展名）
        /// </summary>
        public string ORIG_FILE_NAME { get; set; }
        /// <summary>
        /// 现文件名（文件名.扩展名）
        /// </summary>
        public string RES_FILE_NAME { get; set; }
        /// <summary>
        /// 原始文件大小
        /// </summary>
        public string ORIG_FILE_LEN { get; set; }
        /// <summary>
        /// 现文件大小
        /// </summary>
        public string RES_FILE_LEN { get; set; }
        /// <summary>
        /// 作者对资源说明
        /// </summary>
        public string RES_DESCRIPTION { get; set; }
        /// <summary>
        /// 公开差别
        /// </summary>
        public string FLAG_PUBLIC { get; set; }
        /// <summary>
        /// 总阅读量
        /// </summary>
        public long? TOTAL_READ { get; set; }
        /// <summary>
        /// 评论判别
        /// </summary>
        public string FALG_COMMENT { get; set; }
        /// <summary>
        /// 总评论数
        /// </summary>
        public long? TOTAL_COMMENT { get; set; }
        /// <summary>
        /// 下载判别
        /// </summary>
        public string FLAG_DOWNLOAD { get; set; }
        /// <summary>
        /// 总下载量
        /// </summary>
        public long? TOTAL_DOWNLOAD { get; set; }
        /// <summary>
        /// 中间转换文件名
        /// </summary>
        public string CONVERTED_FILE_NAME { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "RES_RESOURCE";
        }
        public override string GetTableName()
        {
            return "RES_RESOURCE";
        }
    }

    /// <summary>
    /// RES_RESOURCE主键
    /// </summary>
    public class PK_RES_RESOURCE
    {
        #region 属性
        /// <summary>
        /// 资源编号
        /// </summary>
        public string RES_NO { get; set; }
        #endregion
    }
}
