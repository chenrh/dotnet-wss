﻿/*
* 张沛@2014-05-09 14:00:15
*/
using System;
using WSS.Pub;
namespace WSS.Models.Res
{
    /// <summary>
    /// RES_PERMISSION模型
    /// </summary>
    public class RES_PERMISSION : BaseModel
    {
        #region 属性
        /// <summary>
        /// 资源编号
        /// </summary>
        public string RES_NO { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string USER_ID { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "RES_PERMISSION";
        }
        public override string GetTableName()
        {
            return "RES_PERMISSION";
        }
    }

    /// <summary>
    /// RES_PERMISSION主键
    /// </summary>
    public class PK_RES_PERMISSION
    {
        #region 属性
        /// <summary>
        /// 资源编号
        /// </summary>
        public string RES_NO { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string USER_ID { get; set; }
        #endregion
    }
}
