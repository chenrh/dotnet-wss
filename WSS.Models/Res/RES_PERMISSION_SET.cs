﻿/*
* 张沛@2014-05-09 13:59:13
*/
using System;
using WSS.Pub;
namespace WSS.Models.Res
{
    /// <summary>
    /// RES_PERMISSION_SET模型
    /// </summary>
    public class RES_PERMISSION_SET : BaseModel
    {
        #region 属性
        /// <summary>
        /// 主键ID
        /// </summary>
        public string PK_ID { get; set; }
        /// <summary>
        /// 资源编号
        /// </summary>
        public string RES_NO { get; set; }
        /// <summary>
        /// 权限类别
        /// </summary>
        public string PER_TYPE { get; set; }
        /// <summary>
        /// 权限描述
        /// </summary>
        public string PER_DESC { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "RES_PERMISSION_SET";
        }
        public override string GetTableName()
        {
            return "RES_PERMISSION_SET";
        }
    }

    /// <summary>
    /// RES_PERMISSION_SET主键
    /// </summary>
    public class PK_RES_PERMISSION_SET
    {
        #region 属性
        /// <summary>
        /// 主键ID
        /// </summary>
        public string PK_ID { get; set; }
        #endregion
    }
}
