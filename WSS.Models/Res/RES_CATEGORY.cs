﻿/*
* 陈日红@2014-04-18 11:05:55 Email:shadowee@qq.com
*/
using System;
using WSS.Pub;

namespace WSS.Models.Res
{
    /// <summary>
    /// RES_CATEGORY模型
    /// </summary>
    public class RES_CATEGORY : BaseModel
    {
        #region 属性
        /// <summary>
        /// 分类编号
        /// </summary>
        public string CATE_NO { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string CATE_NAME { get; set; }
        /// <summary>
        /// 分类上级编号
        /// </summary>
        public string CATE_PNO { get; set; }
        /// <summary>
        /// 作废判别
        /// </summary>
        public int? FLAG_INVALID { get; set; }
        /// <summary>
        /// 排序编号
        /// </summary>
        public int? ORDER_NO { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public string USER_ID { get; set; }
        /// <summary>
        /// 层级
        /// </summary>
        public int? CATE_LEVEL { get; set; }
        /// <summary>
        /// 末级判别
        /// </summary>
        public int? FLAG_LAST { get; set; }
        #endregion

        public override string GetModelName()
        {
            return "RES_CATEGORY";
        }
        public override string GetTableName()
        {
            return "RES_CATEGORY";
        }
    }

    /// <summary>
    /// RES_CATEGORY主键
    /// </summary>
    public class PK_RES_CATEGORY
    {
        #region 属性
        /// <summary>
        /// 分类编号
        /// </summary>
        public string CATE_NO { get; set; }
        #endregion
    }
}
