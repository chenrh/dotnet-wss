﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSS.Pub
{
    public enum ECase
    {
        UPPER,
        LOWER,
        NORMAL
    }

    public enum ESqlType
    {
        INSERT,
        UPDATE,
        DELETE
    }

    public enum ESort
    {
        ASC,
        DESC
    }

    public enum DBSERVER_TYPE
    {
        ORACLE = 1,
        MSSQL = 2,
        UNDEF = 3
    }


}