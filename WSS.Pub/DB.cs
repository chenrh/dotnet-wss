﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using IBatisNet.DataAccess.Configuration;
using IBatisNet.DataMapper;

namespace WSS.Pub
{
    public static class DB
    {
        // Methods
        public static void Execute(DBState dbstate)
        {
            Execute(new List<DBState> { dbstate });
        }

        public static void Execute(IEnumerable<DBState> batchStates)
        {
            DBBase.getInstance().Execute(batchStates);
        }

        public static void Execute(List<string> sqls)
        {
            List<DBState> batchStatements = new List<DBState>();
            foreach (string str in sqls)
            {
                DBState item = new DBState { Name = "ExecuteNoneQuery", Param = new { sql = str } };
                batchStatements.Add(item);
            }
            DBBase.getInstance().Execute(batchStatements);
        }

        public static void Execute(string sql)
        {
            DBBase.getInstance().Execute(sql);
        }

        public static void Execute(string conn, string sql)
        {
            DBBase.getInstance(conn).Execute(sql);
        }

        public static DataTable ExecuteMsSqlStoredProcedureWithOutputCursor(string mapid, StrObjDict dictionary, IDictionary<string, ParameterDirection> dict2, out Hashtable ht)
        {
            return DBBase.getInstance().QueryForDataTable(mapid, dictionary, dictionary, dict2, out ht);
        }

        public static DataTable ExecuteMsSqlStoredProcedureWithOutputCursor(string conn, string mapid, StrObjDict dictionary, IDictionary<string, ParameterDirection> dict2, out Hashtable ht)
        {
            return DBBase.getInstance(conn).QueryForDataTable(mapid, dictionary, dictionary, dict2, out ht);
        }

        public static void ExecuteOracleStoredProcedureWithOutputCursor(string mapid, IDictionary<string, object> dictionary)
        {
            DBBase.getInstance().ExecuteOracleStoredProcedureWithOutputCursor(mapid, dictionary);
        }

        public static void ExecuteOracleStoredProcedureWithOutputCursor(string conn, string mapid, IDictionary<string, object> dictionary)
        {
            DBBase.getInstance(conn).ExecuteOracleStoredProcedureWithOutputCursor(mapid, dictionary);
        }

        public static void ExecuteWithTransaction(ISqlMapper sqlmapper, IEnumerable<DBState> batchStates)
        {
            DBBase.getInstance().ExecuteWithTransaction(sqlmapper, batchStates);
        }

        public static DBBase GetDbBase()
        {
            return DBBase.getInstance(null);
        }

        public static DBSERVER_TYPE GetDbtype()
        {
            return GetDbtype("");
        }

        public static DBSERVER_TYPE GetDbtype(string conn)
        {
            if ((DBBase.getInstance(conn).GetDbtype().IndexOf("sqlServer") == -1) && (DBBase.getInstance(conn).GetDbtype().IndexOf("oracle") != -1))
            {
                return DBSERVER_TYPE.ORACLE;
            }
            return DBSERVER_TYPE.MSSQL;
        }

        public static string GetSql(string mapid, object obj)
        {
            if (obj is StrObjDict)
            {
                return DBBase.getInstance(null).GetSql(mapid, obj, false);
            }
            return DBBase.getInstance(null).GetSql(mapid, obj.ToStrObjDict(false), false);
        }

        public static IList<T> List<T>(IDictionary<string, object> dictionary) where T : BaseModel, new()
        {
            return List<T>(null, dictionary);
        }

        public static IList<T> List<T>(string conn, IDictionary<string, object> dictionary) where T : BaseModel, new()
        {
            return DBBase.getInstance(conn).ExecuteQueryForList<T>(Activator.CreateInstance<T>().MAP_LIST, dictionary);
        }

        public static DataTable ListDataTable(string sql)
        {
            return DBBase.getInstance().QueryForDataTable("ExecuteForQuery", new { sql = sql });
        }

        public static IList<StrObjDict> ListSod(string mapid, IDictionary<string, object> dictionary)
        {
            return ListSod(null, mapid, dictionary);
        }

        public static IList<StrObjDict> ListSod(string mapid, object obj)
        {
            return ListSod(null, mapid, obj.ToStrObjDict(false));
        }

        public static IList<StrObjDict> ListSod(string conn, string mapid, IDictionary<string, object> dictionary)
        {
            return DBBase.getInstance(conn).ExecuteQueryForList<StrObjDict>(mapid, dictionary);
        }

        public static T Load<T, V>(V v) where T : BaseModel, new()
        {
            // This item is obfuscated and can not be translated.
            T local = Activator.CreateInstance<T>();
            T local1 = DBBase.getInstance().ExecuteQueryForObject<T>(local.MAP_LOAD, v);
            if (local1 != null)
            {
                return local1;
            }
            return local;
        }

        public static IList<StrObjDict> Select(string sql)
        {
            return Select(null, sql);
        }

        public static IList<StrObjDict> Select(string conn, string sql)
        {
            return DBBase.getInstance(conn).ExecuteQueryForList<StrObjDict>("ExecuteForQuery", new { sql = sql });
        }

        public static DataTable SelectDataTable(string sql)
        {
            return SelectDataTable(null, sql);
        }

        public static DataTable SelectDataTable(string conn, string sql)
        {
            return DBBase.getInstance(conn).QueryForDataTable("ExecuteForQuery", new { sql = sql });
        }

        public static object SelectFirst(string sql)
        {
            return DBBase.getInstance().ExecuteScalar("ExecuteForQuery", new { sql = sql });
        }

        public static StrObjDict SelectRow(string sql)
        {
            IList<StrObjDict> source = Select(null, sql);
            if (source.Count > 0)
            {
                return source.FirstOrDefault<StrObjDict>();
            }
            return null;
        }
    }
}