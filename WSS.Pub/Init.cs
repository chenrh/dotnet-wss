﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBatisNet.DataAccess.Configuration;

namespace WSS.Pub
{
    public static class Init
    {
        public static void InitDao()
        {
            DomDaoManagerBuilder dao = new DomDaoManagerBuilder();
            dao.Configure();
        }
    }
}