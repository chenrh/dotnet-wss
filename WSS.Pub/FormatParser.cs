﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace WSS.Pub
{
    public static class FormatParser
    {
        // Fields
        public static string DEFAULT_CAPTURE_RULE = "%([A-Za-z_]*)%";

        // Methods
        public static IDictionary<string, string> CaptureFormatKey(string formatString, string captureRule)
        {
            captureRule = string.IsNullOrEmpty(captureRule) ? DEFAULT_CAPTURE_RULE : captureRule;
            MatchCollection matchs = new Regex(captureRule).Matches(formatString);
            IDictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (Match match in matchs)
            {
                dictionary.Add(match.Groups[1].Value.ToUpper(), match.Value);
            }
            return dictionary;
        }
    }
}