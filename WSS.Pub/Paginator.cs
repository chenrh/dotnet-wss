﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace WSS.Pub
{
    public static class Paginator
    {
        private static string Req(string key)
        {
            return System.Web.HttpContext.Current.Request[key];
        }

        public static object Page<T>(IList<T> datas)
        {
            int pagenum = int.Parse(Req("pagenum"));
            int pagesize = int.Parse(Req("pagesize"));

            List<T> list = datas.Skip((pagenum - 1) * pagesize).Take(pagesize).ToList();

            decimal count = datas.Count;
            decimal pages = Math.Ceiling(count / pagesize);

            return new { page = pages != 0 ? pagenum : 1, size = pages != 0 ? pages : 1, rows = list };
        }
    }
}