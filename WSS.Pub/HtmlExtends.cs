﻿using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace WSS.Pub
{
    public static class HtmlExtends
    {

        #region 按钮
        private const string _button = "<button type='button' id='{0}' class='{1}'>{2}</button>";
        private static IHtmlString _MyButton(HtmlHelper html, string id, string cls, string text)
        {
            return html.Raw(string.Format(_button, id, cls, text));
        }
        public static IHtmlString MyButton(this HtmlHelper html, string id, string text)
        {
            string css = "btn btn-default";
            return _MyButton(html, id, css, text);
        }
        public static IHtmlString MyButton(this HtmlHelper html, string id, string text, string level)
        {
            string css = "btn btn-" + level;
            return _MyButton(html, id, css, text);
        }
        public static IHtmlString MyButton(this HtmlHelper html, string id, string text, string level, string size)
        {
            string css = "btn btn-" + level + " btn-" + size;
            return _MyButton(html, id, css, text);
        }
        public static IHtmlString MyLinkButton(this HtmlHelper html, string id, string text)
        {
            return html.Raw(string.Format(_button, id, "btn btn-link", text));
        }
        #endregion

        #region 标签
        private const string _label = "<span class='{0}'>{1}</span>";
        private static IHtmlString _MyLabel(HtmlHelper html, string cls, string text)
        {
            return html.Raw(string.Format(_label, cls, text));
        }
        public static IHtmlString MyLabel(this HtmlHelper html, string text)
        {
            return MyLabel(html, "default", text);
        }
        public static IHtmlString MyLabel(this HtmlHelper html, string text, string level)
        {
            string css = "label label-" + level;
            return _MyLabel(html, css, text);
        }
        #endregion

    }
}