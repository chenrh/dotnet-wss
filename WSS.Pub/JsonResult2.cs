﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace WSS.Pub
{
    public class JsonResult2 : JsonResult
    {
        // Methods
        private JavaScriptSerializer CreateJsonSerializer()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength *= 100;
            return serializer;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = !string.IsNullOrEmpty(base.ContentType) ? base.ContentType : "application/json";
            if (base.ContentEncoding != null)
            {
                response.ContentEncoding = base.ContentEncoding;
            }
            if (base.Data != null)
            {
                JavaScriptSerializer serializer = this.CreateJsonSerializer();
                response.Write(serializer.Serialize(base.Data));
            }
        }
    }
}