﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
namespace WSS.Pub
{
    public abstract class BaseModel
    {
        // Methods
        public T Bind<T>(IDictionary<string, object> dictionary) where T : BaseModel
        {
            PropertyInfo[] infoArray = this.GetPublic_Instance_DeclaredOnly_PropertyInfo();
            foreach (PropertyInfo info in infoArray)
            {
                if (dictionary.Keys.Contains(info.Name))
                {
                    string str = dictionary[info.Name].ToString();
                    Type propertyType = info.PropertyType;
                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    {
                        propertyType = Nullable.GetUnderlyingType(propertyType);
                    }
                    if (!string.IsNullOrEmpty(str))
                    {
                        object obj2 = Convert.ChangeType(str, propertyType);
                        info.SetValue(this, obj2, null);
                    }
                    else
                    {
                        info.SetValue(this, null, null);
                    }
                }
                else
                {
                    info.SetValue(this, null, null);
                }
            }
            return (this as T);
        }

        public IDictionary<string, object> Bind(object o)
        {
            return StrObjDict.FromVariable(this).Merger(o);
        }

        public T Bind<T>(HttpRequestBase request) where T : BaseModel
        {
            PropertyInfo[] infoArray = this.GetPublic_Instance_DeclaredOnly_PropertyInfo();
            foreach (PropertyInfo info in infoArray)
            {
                string str = request.Req(info.Name, null);
                Type propertyType = info.PropertyType;
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                {
                    propertyType = Nullable.GetUnderlyingType(propertyType);
                }
                if (!string.IsNullOrEmpty(str))
                {
                    object obj2 = Convert.ChangeType(str, propertyType);
                    info.SetValue(this, obj2, null);
                }
                else
                {
                    info.SetValue(this, null, null);
                }
            }
            return (this as T);
        }

        public IDictionary<string, object> Bind(HttpRequestBase request)
        {
            return StrObjDict.FromVariable(this.Bind<BaseModel>(request));
        }

        public abstract string GetModelName();
        public abstract string GetTableName();
        public StrObjDict ToDict()
        {
            return this.ToDict(true);
        }

        public StrObjDict ToDict(bool nullValueAsKey)
        {
            return this.ToDict(nullValueAsKey, ECase.UPPER);
        }

        public StrObjDict ToDict(bool nullValueAsKey, ECase keyCaseSensitive)
        {
            return StrObjDict.FromVariable(this, nullValueAsKey, keyCaseSensitive);
        }

        // Properties
        public virtual string MAP_DELETE
        {
            get
            {
                return ("DELETE_" + this.GetTableName());
            }
        }

        public virtual string MAP_INSERT
        {
            get
            {
                return ("INSERT_" + this.GetTableName());
            }
        }

        public virtual string MAP_LIST
        {
            get
            {
                return ("LIST_" + this.GetTableName());
            }
        }

        public virtual string MAP_LIST2
        {
            get
            {
                return ("LIST2_" + this.GetTableName());
            }
        }

        public virtual string MAP_LOAD
        {
            get
            {
                return ("LOAD_" + this.GetTableName());
            }
        }

        public virtual string MAP_UPDATE
        {
            get
            {
                return ("UPDATE_" + this.GetTableName());
            }
        }
    }
}