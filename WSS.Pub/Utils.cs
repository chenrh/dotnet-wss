﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace WSS.Pub
{
    public static class Utils
    {
        // Methods
        public static string Escape(string str)
        {
            StringBuilder builder = new StringBuilder();
            foreach (char ch in str)
            {
                builder.Append((((char.IsLetterOrDigit(ch) || (ch == '-')) || ((ch == '_') || (ch == ' '))) || ((ch == '/') || (ch == '.'))) ? ch.ToString() : Uri.HexEscape(ch));
            }
            return builder.ToString();
        }

        public static EmptyResult ExportExcel(string HtmlString)
        {
            return ExportExcel(HtmlString, "export");
        }

        public static EmptyResult ExportExcel(string HtmlString, string ExcelName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Public);
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.Charset = "UTF-8";
            HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(ExcelName, Encoding.UTF8) + ".xls");
            HttpContext.Current.Response.ContentEncoding = Encoding.GetEncoding("UTF-8");
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write("<html><head><meta http-equiv=Content-Type content='text/html; charset=utf-8'></head><body>");
            HttpContext.Current.Response.Write(HtmlString);
            HttpContext.Current.Response.Write("</body></html>");
            HttpContext.Current.Response.End();
            return new EmptyResult();
        }

        public static string FillLeftString(string a_str, char a_char, int a_len)
        {
            if (a_str.Length <= a_len)
            {
                int num = a_len - a_str.Length;
                for (int i = 0; i < num; i++)
                {
                    a_str = a_char + a_str;
                }
            }
            return a_str;
        }

        private static void getorder1(ref string OrderByStr, string COL, string keyword)
        {
            if (!string.IsNullOrEmpty(COL))
            {
                string str = OrderByStr;
                OrderByStr = str + " WHEN  CHARINDEX( UPPER(" + COL + ") , '" + keyword + "')>0 THEN  CHARINDEX( UPPER(" + COL + ") , '" + keyword + "') ";
            }
        }

        private static void getorder2(ref string OrderByStr, string COL, string keyword)
        {
            if (!string.IsNullOrEmpty(COL))
            {
                string str = OrderByStr;
                OrderByStr = str + " WHEN  CHARINDEX( UPPER(" + COL + ") , '" + keyword + "')>0 THEN  " + COL + " ";
            }
        }

        public static string GetPageSql_Ora(string Sql, int DataCount, int Page, int PageRows)
        {
            return string.Concat(new object[] { "SELECT *  FROM (SELECT ROWNUM AS NUM, x.*  FROM (", Sql, ") x ) WHERE NUM > ", (Page - 1) * PageRows, " AND NUM<=", Page * PageRows });
        }

        public static string GetSrmLike(string srm, string keyword, string SRM_COL, string NAME_COL, string CODE_COL, string OTHER_COL)
        {
            string str5;
            string str = "";
            string str2 = srm;
            keyword = keyword.ToUpper();
            string str3 = srm.ToUpper();
            if (string.IsNullOrEmpty(str2))
            {
                str3 = "SRM1";
            }
            str3 = str3.Substring(str3.Length - 1);
            if (!string.IsNullOrEmpty(SRM_COL))
            {
                str5 = str;
                str = str5 + " or  UPPER(" + SRM_COL + str3 + ") LIKE '%" + keyword + "%'";
            }
            if (!string.IsNullOrEmpty(NAME_COL))
            {
                str5 = str;
                str = str5 + " or UPPER(" + NAME_COL + ") LIKE '%" + keyword + "%'";
            }
            if (!string.IsNullOrEmpty(CODE_COL))
            {
                str5 = str;
                str = str5 + " or UPPER(" + CODE_COL + ") LIKE '%" + keyword + "%'";
            }
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                string[] strArray = OTHER_COL.Split(new char[] { ',' });
                for (int i = 0; i < strArray.Length; i++)
                {
                    str5 = str;
                    str = str5 + " or UPPER(" + strArray[i] + ") LIKE '%" + keyword + "%'";
                }
            }
            return (" ( 1=0 " + str + ") ");
        }

        public static string GetSrmOrderBy_Mssql(string SRM, string KEYWORD, string INPUT_CODE_COL, string NAME_COL, string CODE_COL, string OTHER_COL)
        {
            string[] strArray;
            int num;
            string orderByStr = " CAST((CASE ";
            string str2 = SRM;
            KEYWORD = KEYWORD.ToUpper();
            INPUT_CODE_COL = INPUT_CODE_COL.ToUpper();
            if (string.IsNullOrEmpty(str2))
            {
                str2 = "SRM1";
            }
            str2 = str2.Substring(str2.Length - 1);
            getorder1(ref orderByStr, INPUT_CODE_COL + str2, KEYWORD);
            if (INPUT_CODE_COL.IndexOf("SRM") >= 0)
            {
                getorder1(ref orderByStr, INPUT_CODE_COL + "3", KEYWORD);
            }
            getorder1(ref orderByStr, NAME_COL, KEYWORD);
            getorder1(ref orderByStr, CODE_COL, KEYWORD);
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    getorder1(ref orderByStr, strArray[num], KEYWORD);
                }
            }
            orderByStr = orderByStr + " END) as varchar) " + " + '_' + CAST(len(CASE ";
            getorder2(ref orderByStr, INPUT_CODE_COL + str2, KEYWORD);
            if (INPUT_CODE_COL.IndexOf("SRM") >= 0)
            {
                getorder2(ref orderByStr, INPUT_CODE_COL + "3", KEYWORD);
            }
            getorder2(ref orderByStr, NAME_COL, KEYWORD);
            getorder2(ref orderByStr, CODE_COL, KEYWORD);
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    getorder2(ref orderByStr, strArray[num], KEYWORD);
                }
            }
            orderByStr = orderByStr + " END) as varchar ) " + " + '_' +  (CASE ";
            getorder2(ref orderByStr, INPUT_CODE_COL + str2, KEYWORD);
            if (INPUT_CODE_COL.IndexOf("SRM") >= 0)
            {
                getorder2(ref orderByStr, INPUT_CODE_COL + "3", KEYWORD);
            }
            getorder2(ref orderByStr, NAME_COL, KEYWORD);
            getorder2(ref orderByStr, CODE_COL, KEYWORD);
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    getorder2(ref orderByStr, strArray[num], KEYWORD);
                }
            }
            orderByStr = orderByStr + " END) ";
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    orderByStr = orderByStr + " + '_' +" + strArray[num];
                }
                return orderByStr;
            }
            if (!string.IsNullOrEmpty(NAME_COL))
            {
                orderByStr = orderByStr + " +  '_' + " + NAME_COL;
            }
            if (!string.IsNullOrEmpty(CODE_COL))
            {
                orderByStr = orderByStr + " +  '_' + " + CODE_COL;
            }
            return orderByStr;
        }

        public static string GetSrmOrderBy_Ora(string SRM, string KEYWORD, string INPUT_CODE_COL, string NAME_COL, string CODE_COL, string OTHER_COL)
        {
            string[] strArray;
            int num;
            string orderByStr = " TO_CHAR((CASE ";
            string str2 = SRM;
            KEYWORD = KEYWORD.ToUpper();
            INPUT_CODE_COL = INPUT_CODE_COL.ToUpper();
            if (string.IsNullOrEmpty(str2))
            {
                str2 = "SRM1";
            }
            str2 = str2.Substring(str2.Length - 1);
            getorder1(ref orderByStr, INPUT_CODE_COL + str2, KEYWORD);
            if (INPUT_CODE_COL.IndexOf("SRM") >= 0)
            {
                getorder1(ref orderByStr, INPUT_CODE_COL + "3", KEYWORD);
            }
            getorder1(ref orderByStr, NAME_COL, KEYWORD);
            getorder1(ref orderByStr, CODE_COL, KEYWORD);
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    getorder1(ref orderByStr, strArray[num], KEYWORD);
                }
            }
            orderByStr = orderByStr + " END),'00') " + " || '_' || TO_CHAR(length(CASE ";
            getorder2(ref orderByStr, INPUT_CODE_COL + str2, KEYWORD);
            if (INPUT_CODE_COL.IndexOf("SRM") >= 0)
            {
                getorder2(ref orderByStr, INPUT_CODE_COL + "3", KEYWORD);
            }
            getorder2(ref orderByStr, NAME_COL, KEYWORD);
            getorder2(ref orderByStr, CODE_COL, KEYWORD);
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    getorder2(ref orderByStr, strArray[num], KEYWORD);
                }
            }
            orderByStr = orderByStr + " END),'000') " + " || '_' ||  (CASE ";
            getorder2(ref orderByStr, INPUT_CODE_COL + str2, KEYWORD);
            if (INPUT_CODE_COL.IndexOf("SRM") >= 0)
            {
                getorder2(ref orderByStr, INPUT_CODE_COL + "3", KEYWORD);
            }
            getorder2(ref orderByStr, NAME_COL, KEYWORD);
            getorder2(ref orderByStr, CODE_COL, KEYWORD);
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    getorder2(ref orderByStr, strArray[num], KEYWORD);
                }
            }
            orderByStr = orderByStr + " END) ";
            if (!string.IsNullOrEmpty(OTHER_COL))
            {
                strArray = OTHER_COL.Split(new char[] { ',' });
                for (num = 0; num < strArray.Length; num++)
                {
                    orderByStr = orderByStr + " || '_' ||" + strArray[num];
                }
                return orderByStr;
            }
            if (!string.IsNullOrEmpty(NAME_COL))
            {
                orderByStr = orderByStr + " ||  '_' || " + NAME_COL;
            }
            if (!string.IsNullOrEmpty(CODE_COL))
            {
                orderByStr = orderByStr + " ||  '_' || " + CODE_COL;
            }
            return orderByStr;
        }

        public static string GetString(object as_str)
        {
            if ((as_str == DBNull.Value) || (as_str == null))
            {
                return "";
            }
            return as_str.ToString();
        }

        public static string GetString(StrObjDict dict, string as_str)
        {
            if (dict.ContainsKey(as_str))
            {
                return GetString(dict[as_str]);
            }
            return "";
        }

        public static string GetClientIP()
        {
            return System.Web.HttpContext.Current.Request.ServerVariables.Get("REMOTE_ADDR");
            //return System.Web.HttpContext.Current.Request.UserHostAddress;
        }

        public static byte[] StringToUTF8ByteArray(string pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetBytes(pXmlString);
        }

        public static string UTF8ByteArrayToString(byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetString(characters);
        }
    }
}