﻿using System;
using System.Collections.Generic;

namespace WSS.Pub
{
    [Serializable]
    public class StrObjDict : Dictionary<string, object>
    {
        // Methods
        public StrObjDict()
        {
        }

        public StrObjDict(StrObjDict dictionary)
            : base(dictionary)
        {
        }

        public static StrObjDict FromVariable(object o)
        {
            return FromVariable(o, true);
        }

        public static StrObjDict FromVariable(object o, bool nullValueAsKey)
        {
            return FromVariable(o, nullValueAsKey, ECase.UPPER);
        }

        public static StrObjDict FromVariable(object o, bool nullValueAsKey, ECase keyCaseSensitive)
        {
            return (new StrObjDict().Merger(o, nullValueAsKey, keyCaseSensitive) as StrObjDict);
        }
    }

}