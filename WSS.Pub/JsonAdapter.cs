﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace WSS.Pub
{
    public static class JsonAdapter
    {
        // Methods
        public static object FromJsonAsDictionary(string jsonstr)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength *= 100;
            if (jsonstr == null)
            {
                jsonstr = "";
            }
            return (serializer.DeserializeObject(jsonstr) ?? new StrObjDict());
        }

        public static string ToJson(object o)
        {
            string str = "";
            if (o != null)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength *= 100;
                str = serializer.Serialize(o);
            }
            return str;
        }
    }


}