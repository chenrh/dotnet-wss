﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSS.Pub
{
    public class MyComparer<T> : IComparer<T>
    {
        // Fields
        private Func<T, T, int> _MyCompare;

        // Methods
        public MyComparer(Func<T, T, int> compare)
        {
            this._MyCompare = compare;
        }

        public int Compare(T x, T y)
        {
            return this._MyCompare(x, y);
        }
    }



}